﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.View;

namespace TrainDataBase
{
    public partial class MainMenuForm : Form
    {
        OracleConnection connection;
        MainForm mainForm;

        public MainMenuForm(MainForm mainForm, OracleConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
            this.mainForm = mainForm;
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {

        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            this.Close();
            var connectionForm = new ConnectForm(mainForm);
            connectionForm.MdiParent = mainForm;
            connectionForm.Show();
        }

        private void MainMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void createTablesButton_Click(object sender, EventArgs e)
        {
            if (DBOracleUtils.CreateTables(connection))
            {
                MessageBox.Show("Таблицы успешно созданы!");
            }
            else
            {
                MessageBox.Show("Таблицы не были созданы.");
            };
        }

        private void deleteTablesButton_Click(object sender, EventArgs e)
        {
            if (DBOracleUtils.DeleteTables(connection))
            {
                MessageBox.Show("Таблицы успешно удалены!");
            }
            else
            {
                MessageBox.Show("Таблицы не были удалены.");
            };
        }

        private void fillTablesButton_Click(object sender, EventArgs e)
        {
            if (DBOracleUtils.InsertData(connection))
            {
                MessageBox.Show("Таблицы успешно заполнены!");
            }
            else
            {
                MessageBox.Show("Таблицы не были заполнены.");
            };
        }

        private void editTablesButton_Click(object sender, EventArgs e)
        {
            TableListForm tlf = new TableListForm(mainForm, connection);
            tlf.MdiParent = mainForm;
            tlf.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBOracleUtils.CountStations(connection,0,0,0);
        }

        private void quariesButton_Click(object sender, EventArgs e)
        {
            QueryListForm qlf = new QueryListForm(mainForm, connection);
            qlf.MdiParent = mainForm;
            qlf.Show();
            this.Close();
        }

        private void specialFunctionsButton_Click(object sender, EventArgs e)
        {
            SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
            sffl.MdiParent = mainForm;
            sffl.Show();
            this.Close();
        }
    }
}
