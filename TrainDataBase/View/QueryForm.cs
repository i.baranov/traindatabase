﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.View
{
    public partial class QueryForm : Form
    {
        MainForm mainForm;
        OracleConnection connection;
        List<Control> controls = new List<Control>();
        List<DbTypes> types = new List<DbTypes>();
        Query table;
        int id = 0;

        public QueryForm(MainForm mainForm, OracleConnection connection, Table table)
        {
            InitializeComponent();
            this.connection = connection;
            this.table = (Query) table;
            queryLabel.Text = table.viewName;
            this.mainForm = mainForm;
        }

        private string getViewName(Reference reference, string keyValue)
        {
            List<string> items = new List<string>();

            var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValue, reference.refFieldName, reference.viewFields);
            string viewName = "";
            for (int j = 0; j < viewNames.Count; j++)
            {
                if (j > 0) viewName += " ";
                viewName += viewNames[j];
            }

            return viewName;
        }

        private void addQueryParams()
        {
            addFieldsToPanel(table.queryParams, table.defaultValues);
        }

        private void addFieldsToPanel(List<QueryParametr> fields, List<string> values)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                addOneField(fields[i], values[i], id);
                id += 2;
            }
        }

        private Label addFieldLabel(Field field, int id)
        {
            Label fieldName = new Label();
            fieldName.Location = new System.Drawing.Point(10, 25 * id + 5);
            fieldName.Name = "lB" + (id).ToString();
            fieldName.Size = new System.Drawing.Size(300, 20);
            fieldName.Text = field.viewName;
            paramsPanel.Controls.Add(fieldName);
            return fieldName;
        }

        private ComboBox addDropDownList(Field field, int id, string[] items)
        {
            ComboBox dropDown = new ComboBox();
            dropDown.Location = new Point(10, 25 * (id + 1) + 5);
            dropDown.Name = "cB" + (id + 1).ToString();
            dropDown.Size = new Size(130, 20);
            dropDown.Items.AddRange(items);
            dropDown.Enabled = field.editable;
            dropDown.DropDownStyle = ComboBoxStyle.DropDownList;
            paramsPanel.Controls.Add(dropDown);
            controls.Add(dropDown);
            return dropDown;
        }

        private DateTimePicker addDatePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(100, 20);
            dateTimePicker.CustomFormat = "dd-MM-yyyy";
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            dateTimePicker.Enabled = field.editable;
            paramsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
            return dateTimePicker;
        }

        private TextBox addTextBox(Field field, int id)
        {
            TextBox fieldTextBox = new System.Windows.Forms.TextBox();
            fieldTextBox.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            fieldTextBox.Name = "tB" + (id + 1).ToString();
            fieldTextBox.Size = new System.Drawing.Size(100, 20);
            fieldTextBox.Enabled = field.editable;
            paramsPanel.Controls.Add(fieldTextBox);
            controls.Add(fieldTextBox);
            return fieldTextBox;
        }

        private NumericUpDown addNumericUpDown(Field field, int id)
        {
            NumericUpDown numericUpDown = new NumericUpDown();
            numericUpDown.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            numericUpDown.Name = "nUD" + (id + 1).ToString();
            numericUpDown.Size = new System.Drawing.Size(100, 20);
            numericUpDown.Maximum = 1000000000;
            paramsPanel.Controls.Add(numericUpDown);
            numericUpDown.Enabled = field.editable;
            controls.Add(numericUpDown);
            return numericUpDown;
        }

        private string[] getViewNames(Reference reference)
        {
            var keyValues = DBOracleUtils.SelectEntity(connection,
                        reference.tableName,
                        reference.refFieldName);

            List<string> items = new List<string>();
            for (int i = 0; i < keyValues.Count; i++)
            {
                var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValues[i], reference.refFieldName, reference.viewFields);
                string viewName = "";
                for (int j = 0; j < viewNames.Count; j++)
                {
                    if (j > 0) viewName += " ";
                    viewName += viewNames[j];
                }
                items.Add(viewName);
            }

            items = items.Distinct().ToList();
            return items.ToArray();
        }

        private DateTimePicker addTimePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(100, 20);
            //dateTimePicker.Format = DateTimePickerFormat.Long;
            dateTimePicker.CustomFormat = "yyyy-MM-dd hh:m"; //YYYY-MM-DD HH24:MI
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            paramsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
            return dateTimePicker;
        }

        private void addOneField(Field field, string value, int id)
        {
            switch (field.type)
            {
                case DbTypes.DbKey:
                    this.id -= 2;
                    types.Add(DbTypes.DbKey);
                    controls.Add(new Label());
                    break;
                case DbTypes.DbEmpty:
                    this.id -= 2;
                    types.Add(DbTypes.DbEmpty);
                    controls.Add(new Label());
                    break;
                case DbTypes.DbRefference:
                    addFieldLabel(field, id);
                    var items = getViewNames(field.reference);
                    var dbList = addDropDownList(field,
                        id,
                        items);
                    dbList.Text = getViewName(field.reference, value);
                    types.Add(DbTypes.DbRefference);
                    break;
                case DbTypes.DbGender:
                    addFieldLabel(field, id);
                    var dblist =
                        addDropDownList(field, id, new string[] { "Муж.", "Жен." });
                    dblist.Text = value;
                    types.Add(DbTypes.DbGender);
                    break;
                case DbTypes.DbTime:
                    addFieldLabel(field, id);
                    var tpicker =
                        addTimePicker(field, id);
                    tpicker.Text = value;
                    types.Add(DbTypes.DbTime);
                    break;
                case DbTypes.DbDate:
                    addFieldLabel(field, id);
                    var dataPicker = addDatePicker(field, id);
                    try
                    {
                        dataPicker.Text = value;
                    }
                    catch
                    {
                        dataPicker.Checked = false;
                    }
                    types.Add(DbTypes.DbDate);
                    break;
                case DbTypes.DbBool:
                    addFieldLabel(field, id);
                    var dblistBool = addDropDownList(field, id, new string[] { "Да", "Нет" });
                    dblistBool.Text = value;
                    types.Add(DbTypes.DbBool);
                    break;
                case DbTypes.DbComparison:
                    addFieldLabel(field, id);
                    var dblistComp= addDropDownList(field, id, new string[] { "Да", "Нет" });
                    dblistComp.Text = value;
                    types.Add(DbTypes.DbComparison);
                    break;
                case DbTypes.DbInt:
                    addFieldLabel(field, id);
                    var numericUpDown = addNumericUpDown(field, id);
                    numericUpDown.Value = Convert.ToDecimal(value);
                    types.Add(DbTypes.DbInt);
                    break;
                default:
                    addFieldLabel(field, id);
                    var textBox = addTextBox(field, id);
                    textBox.Text = value;
                    types.Add(DbTypes.DbVarchar);
                    break;
            }
        }

        private void addDataToTable(string source)
        {
            List<List<string>> columns = new List<List<string>>();
            tableView.ColumnCount = 0;
            int size = table.fields.Count;

            int rowsCount = DBOracleUtils.GetCountOfRows(connection, source);
            overallLabel.Text = "Общее число: " + rowsCount;

            var entities = DBOracleUtils.SelectAllDataFromTable(connection, source, size);

            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < size; j++)
                {

                    if (tableView.Columns.Count < size)
                    {
                        tableView.Columns.Add(table.fields[j].name, table.fields[j].viewName);
                    }
                    if (tableView.Rows.Count < rowsCount)
                    {
                        tableView.Rows.Add();
                    }

                    switch (table.fields[j].type)
                    {
                        case DbTypes.DbGender:
                            tableView[j, i].Value = DbGender.getGender(entities[i * size + j]);
                            break;
                        case DbTypes.DbBool:
                            tableView[j, i].Value = DbBool.getView(entities[i * size + j]);
                            break;
                        case DbTypes.DbComparison:
                            tableView[j, i].Value = DbBool.getView(entities[i * size + j]);
                            break;
                        case DbTypes.DbRefference:
                            tableView[j, i].Value = getViewName(table.fields[j].reference, entities[i * size + j]);
                            break;
                        default:
                            tableView[j, i].Value = entities[i * size + j];
                            break;
                    }
                }
            }
        }

        private string getStringByType(DbTypes type, Control control, Reference reference)
        {
            string valuesString = "";
            switch (type)
            {
                case DbTypes.DbDate:
                    DateTimePicker dtp = (DateTimePicker)control;
                    if (dtp.Checked)
                    {
                        valuesString += "'" + dtp.Text + "'";
                    }
                    else
                    {
                        valuesString += "NULL";
                    }
                    break;
                case DbTypes.DbTime:
                    DateTimePicker dttp = (DateTimePicker)control;
                    if (dttp.Checked)
                    {
                        valuesString += "TO_TIMESTAMP(" + "'" + dttp.Text + "'" + ",'YYYY-MM-DD HH24:MI')";
                    }
                    else
                    {
                        valuesString += "NULL";
                    }
                    break;
                case DbTypes.DbBool:
                    valuesString += "'" + DbBool.getBool(control.Text) + "'";
                    break;
                case DbTypes.DbComparison:
                    valuesString += DbBool.getComp(control.Text);
                    break;
                case DbTypes.DbGender:
                    valuesString += "'" + DbGender.getBool(control.Text) + "'";
                    break;
                case DbTypes.DbInt:
                    valuesString += control.Text;
                    break;
                case DbTypes.DbRefference:
                    valuesString += DBOracleUtils.SelectKeyWithClause(
                        connection, reference.tableName,
                        reference.refFieldName,
                        reference.viewFields,
                        control.Text.Split(' '))[0];
                    break;
                default:
                    valuesString += "'" + control.Text + "'";
                    break;
            }
            return valuesString;
        }

        private void QueryForm_Load(object sender, EventArgs e)
        {
            this.Width = mainForm.Width - 100;
            this.Height = mainForm.Height - 100;
            addQueryParams();
            //addDataToTable(table.name);
            filtrButton.PerformClick();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            var qlf = new QueryListForm(mainForm, connection);
            qlf.MdiParent = mainForm;
            qlf.Show();
            this.Close();
        }

        private void filtrButton_Click(object sender, EventArgs e)
        {
            string queryStringWithParams = table.queryString;
            
            for (int i= controls.Count-1; i>=0; i--)
            {
                queryStringWithParams = queryStringWithParams.Replace(
                    table.paramsRefs[i], 
                    getStringByType(types[i],controls[i], table.queryParams[i].reference));
            }

            addDataToTable(queryStringWithParams);
        }
    }
}
