﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseQueries;

namespace TrainDataBase.View
{
    public partial class QueryListForm : Form
    {
        OracleConnection connection;
        MainForm mainForm;

        public QueryListForm(MainForm mainForm, OracleConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
            this.mainForm = mainForm;
        }

        string[] queryNames = new string[] {
            "Работники жележнодрожной станции указанного отдела",
            "Работники указанного пола",
            "Работники указанного возраста",
            "Работники по стажу работы",
            "Работники по признаку наличия детей",
            "Работники по зарплате",
            "Работники в указанной бригаде",
            "Работники в указанной бригаде указанного отдела",
            "Работники, обслуживающие указанный локомотив",
            "Работники указанной бригады указанного возраста",
            "Суммарна зарплата в бригаде",
            "Водители, прошедшие или непрошедшие медосмотр в указанный год",
            "Водители указанного пола",
            "Локомотивы, находящиеся на станции в указанное время",
            "Локомотив по времени прибытия на станцию",
            "Локомотив по количеству совершённых маршрутов",
            "Локомотивы , прошедшие плановый техосмотр за период",
            "Локомотивы, отправленные в ремонт в указнное время",
            "Локомотивы, ремонтируемые указанное число раз",
            "Локомотивы по возрасту",
            "Поезда на указанном маршруте",
            "Поезда по длительности маршрута",
            "Поезда по цене билета",
            "Отменённые рейсы",
            "Отменённые рейсы в указанном направлении",
            "Отменённые рейсы по указанному маршруту",
            "Задержанные рейсы полностью",
            "Задержанные рейсы по указанной причине",
            "Задержанные рейсы по указанному маршруту",
            "Количество сданных билетов во время всех задержек",
            "Проданные билеты за указанный интервал времени",
            "Среднее количество количество проданных билетов за указанный интервал времени",
            "Проданные билеты на определённый маршрут",
            "Среднее количество количество проданных билетов на определённый маршрут",
            "Проданные билеты по длительности маршрута",
            "Среднее количество количество проданных билетов по длительности маршрута",
            "Проданные билеты по цене билета",
            "Среднее количество количество проданных билетов по цене билета",
            "Маршруты указанной категории",
            "Маршруты, следующие в определённом направлении",
            "Пассажиры на указанном рейсе",
            "Пассажиры, уехавшие в указанный день",
            "Пассажиры, уехавшие за границу в указанный день",
            "Пассажиры по признаку сдачи в багажное отделение",
            "Пассажиры по половому признаку",
            "Пассажиры по возрасту",
            "Невыкупленные билеты на указанный рейс",
            "Невыкупленные билеты в указанный день",
            "Перечень невыкупленных билетов на некоторый маршрут",
            "Общее число сданных билетов на указанный рейс",
            "Общее число сданных билетов на указанный день",
            "Общее число сданных билетов на указанный маршрут"
        };

        private void showTable(Table table)
        {
            QueryForm qb = new QueryForm(mainForm, connection, table);
            qb.MdiParent = mainForm;
            qb.Show();
            this.Close();
        }

        private void QueryListForm_Load(object sender, EventArgs e)
        {
            tableListBox.Items.AddRange(queryNames);
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            MainMenuForm menu = new MainMenuForm(mainForm, connection);
            this.Close();
            menu.MdiParent = mainForm;
            menu.Show();
        }

        private void tableListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tableListBox.SelectedIndex)
            {
                case (int)QueryId.Query1_1:
                    showTable(new Query1_1());
                    break;
                case (int)QueryId.Query1_2:
                    showTable(new Query1_2());
                    break;
                case (int)QueryId.Query1_3:
                    showTable(new Query1_3());
                    break;
                case (int)QueryId.Query1_4:
                    showTable(new Query1_4());
                    break;
                case (int)QueryId.Query1_5:
                    showTable(new Query1_5());
                    break;
                case (int)QueryId.Query1_6:
                    showTable(new Query1_6());
                    break;
                case (int)QueryId.Query2_1:
                    showTable(new Query2_1());
                    break;
                case (int)QueryId.Query2_2:
                    showTable(new Query2_2());
                    break;
                case (int)QueryId.Query2_3:
                    showTable(new Query2_3());
                    break;
                case (int)QueryId.Query2_4:
                    showTable(new Query2_4());
                    break;
                case (int)QueryId.Query2_5:
                    showTable(new Query2_5());
                    break;
                case (int)QueryId.Query3_1:
                    showTable(new Query3_1());
                    break;
                case (int)QueryId.Query3_2:
                    showTable(new Query3_2());
                    break;
                case (int)QueryId.Query4_1:
                    showTable(new Query4_1());
                    break;
                case (int)QueryId.Query4_2:
                    showTable(new Query4_2());
                    break;
                case (int)QueryId.Query4_3:
                    showTable(new Query4_3());
                    break;
                case (int)QueryId.Query5_1:
                    showTable(new Query5_1());
                    break;
                case (int)QueryId.Query5_2:
                    showTable(new Query5_2());
                    break;
                case (int)QueryId.Query5_3:
                    showTable(new Query5_3());
                    break;
                case (int)QueryId.Query5_4:
                    showTable(new Query5_4());
                    break;
                case (int)QueryId.Query6_1:
                    showTable(new Query6_1());
                    break;
                case (int)QueryId.Query6_2:
                    showTable(new Query6_2());
                    break;
                case (int)QueryId.Query6_3:
                    showTable(new Query6_3());
                    break;
                case (int)QueryId.Query7_1:
                    showTable(new Query7_1());
                    break;
                case (int)QueryId.Query7_2:
                    showTable(new Query7_2());
                    break;
                case (int)QueryId.Query7_3:
                    showTable(new Query7_3());
                    break;
                case (int)QueryId.Query8_1:
                    showTable(new Query8_1());
                    break;
                case (int)QueryId.Query8_2:
                    showTable(new Query8_2());
                    break;
                case (int)QueryId.Query8_3:
                    showTable(new Query8_3());
                    break;
                case (int)QueryId.Query8_4:
                    showTable(new Query8_4());
                    break;
                case (int)QueryId.Query9_1:
                    showTable(new Query9_1());
                    break;
                case (int)QueryId.Query9_2:
                    showTable(new Query9_2());
                    break;
                case (int)QueryId.Query9_3:
                    showTable(new Query9_3());
                    break;
                case (int)QueryId.Query9_4:
                    showTable(new Query9_4());
                    break;
                case (int)QueryId.Query9_5:
                    showTable(new Query9_5());
                    break;
                case (int)QueryId.Query9_6:
                    showTable(new Query9_6());
                    break;
                case (int)QueryId.Query9_7:
                    showTable(new Query9_7());
                    break;
                case (int)QueryId.Query9_8:
                    showTable(new Query9_8());
                    break;
                case (int)QueryId.Query10_1:
                    showTable(new Query10_1());
                    break;
                case (int)QueryId.Query10_2:
                    showTable(new Query10_2());
                    break;
                case (int)QueryId.Query11_1:
                    showTable(new Query11_1());
                    break;
                case (int)QueryId.Query11_2:
                    showTable(new Query11_2());
                    break;
                case (int)QueryId.Query11_3:
                    showTable(new Query11_3());
                    break;
                case (int)QueryId.Query11_4:
                    showTable(new Query11_4());
                    break;
                case (int)QueryId.Query11_5:
                    showTable(new Query11_5());
                    break;
                case (int)QueryId.Query11_6:
                    showTable(new Query11_6());
                    break;
                case (int)QueryId.Query12_1:
                    showTable(new Query12_1());
                    break;
                case (int)QueryId.Query12_2:
                    showTable(new Query12_2());
                    break;
                case (int)QueryId.Query12_3:
                    showTable(new Query12_3());
                    break;
                case (int)QueryId.Query13_1:
                    showTable(new Query13_1());
                    break;
                case (int)QueryId.Query13_2:
                    showTable(new Query13_2());
                    break;
                case (int)QueryId.Query13_3:
                    showTable(new Query13_3());
                    break;
                default:
                    break;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
