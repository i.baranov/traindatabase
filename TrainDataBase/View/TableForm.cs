﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.AddForms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;
using TrainDataBase.View;

namespace TrainDataBase
{
    public partial class TableForm : Form
    {
        MainForm mainForm;
        OracleConnection connection;
        Table table;

        public TableForm(MainForm mainForm, OracleConnection connection, Table table)
        {
            InitializeComponent();
            this.connection = connection;
            this.table = table;
            tableLabel.Text = table.viewName;
            this.mainForm = mainForm;
        }

        private string getViewName(Reference reference, string keyValue)
        {
            List<string> items = new List<string>();

            var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValue, reference.refFieldName, reference.viewFields);
            string viewName = "";
            for (int j = 0; j < viewNames.Count; j++)
            {
                if (j > 0) viewName += " ";
                viewName += viewNames[j];
            }

            return viewName;
        }

        private void TableForm_Load(object sender, EventArgs e)
        {
            List<List<string>> columns = new List<List<string>>();
            tableView.ColumnCount = 0;
            int size = table.fields.Count;

            int rowsCount = DBOracleUtils.GetCountOfRows(connection,table.name);
            this.Width = mainForm.Width-100;
            this.Height = mainForm.Height-100;

            var entities = DBOracleUtils.SelectAllDataFromTable(connection, table.name, size);

            for (int i=0; i< rowsCount; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    
                    if (tableView.Columns.Count < size)
                    {
                        tableView.Columns.Add(table.fields[j].name, table.fields[j].viewName);
                    }
                    if (tableView.Rows.Count < rowsCount)
                    {
                        tableView.Rows.Add();
                    }

                    switch (table.fields[j].type)
                    {
                        case DbTypes.DbGender:
                            tableView[j, i].Value = DbGender.getGender(entities[i * size + j]); 
                            break;
                        case DbTypes.DbBool:
                            tableView[j, i].Value = DbBool.getView(entities[i * size + j]);
                            break;
                        case DbTypes.DbRefference:
                            tableView[j, i].Value = getViewName(table.fields[j].reference, entities[i * size + j]);
                            break;
                        default:
                            tableView[j, i].Value = entities[i * size + j];
                            break;
                    }
                }
            }

            tableView.Columns.Add(new DataGridViewButtonColumn());
            tableView.Columns.Add(new DataGridViewButtonColumn());
            for (int i=0; i<rowsCount; i++)
            {
                var editButtonCell = new DataGridViewButtonCell();
                editButtonCell.Value = "Изменить";
                tableView[size, i] = editButtonCell;

                var deleteButtonCell = new DataGridViewButtonCell();
                deleteButtonCell.Value = "Удалить";
                tableView[size+1, i] = deleteButtonCell;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(mainForm, table.fields, connection, table);
            addForm.MdiParent = mainForm;
            addForm.Show();
            this.Close();
        }

        private void tableView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            int size = table.fields.Count;
            int columnIndex = e.ColumnIndex;

            if (senderGrid.Columns[columnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0 && columnIndex == size)
            {
                List<string> values = new List<string>();
                for (int i=0; i<size; i++)
                {
                    if (table.fields[i].type != DbTypes.DbRefference)
                    {
                        values.Add((string)tableView[i, e.RowIndex].Value);
                    }
                    else
                    {
                        values.Add(DBOracleUtils.SelectKeyWithClause(
                            connection, table.fields[i].reference.tableName,
                            table.fields[i].reference.refFieldName,
                            table.fields[i].reference.viewFields,
                            ((string)tableView[i, e.RowIndex].Value).Split(' '))[0]);
                    }
                }
                EditForm editForm = new EditForm(mainForm, table.fields, values, connection, table);
                editForm.MdiParent = mainForm;
                editForm.Show();
                this.Close();
                return;
            }

            if (senderGrid.Columns[columnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0 && columnIndex == size+1)
            {
                List<string> values = new List<string>();
                for (int i = 0; i < size; i++)
                {
                    values.Add((string)tableView[i, e.RowIndex].Value);
                }
                if (MessageBox.Show("Вы действительно хотите удалить выбраную строку?", "Внимание", MessageBoxButtons.YesNo,
                 MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    string valuesString = values[0];
                    if (table.fields[0].type == DbTypes.DbRefference)
                    {
                        valuesString = DBOracleUtils.SelectKeyWithClause(
                            connection, table.fields[0].reference.tableName,
                            table.fields[0].reference.refFieldName,
                            table.fields[0].reference.viewFields,
                            values[0].Split(' '))[0];
                    }

                    DBOracleUtils.DeleteRow(connection, table.name, table.fields[0].name, valuesString);
                    var tf = new TableForm(mainForm, connection, table);
                    tf.MdiParent = mainForm; 
                    tf.Show();
                    this.Close();
                    return;
                }
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            var tlf = new TableListForm(mainForm, connection);
            tlf.MdiParent = mainForm;
            tlf.Show();
            this.Close();
        }
    }
}
