﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.View;

namespace TrainDataBase
{
    public partial class ConnectForm : Form
    {
        MainForm mainForm;

        public ConnectForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
          //  this.WindowState = FormWindowState.Maximized;
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            // DBOracleUtils.GetDBConnection("localhost", 1521, "SYS$USERS", "Admin", "nyp1gzah");
            //var conn = DBOracleUtils.GetDBConnection();
            var connection = DBOracleUtils.GetDBConnection(userNameTb.Text, passwordTb.Text);

            Console.WriteLine("Get Connection: " + connection);
            try
            {
                connection.Open();

                Console.WriteLine(connection.ConnectionString, "Successful Connection");
                MainMenuForm menu = new MainMenuForm(mainForm,connection);
                this.Close();
                //menu.ShowDialog(this);
                menu.MdiParent = mainForm;
                menu.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine("## ERROR: " + ex.Message);
                return;
            }
        }

        private void ConnectForm_Load(object sender, EventArgs e)
        {

        }

        private void ConnectForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
