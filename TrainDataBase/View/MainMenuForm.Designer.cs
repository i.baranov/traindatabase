﻿namespace TrainDataBase
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.quariesButton = new System.Windows.Forms.Button();
            this.editTablesButton = new System.Windows.Forms.Button();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.specialFunctionsButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(390, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добро пожаловать!";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.specialFunctionsButton);
            this.panel1.Controls.Add(this.quariesButton);
            this.panel1.Controls.Add(this.editTablesButton);
            this.panel1.Location = new System.Drawing.Point(12, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(382, 292);
            this.panel1.TabIndex = 1;
            // 
            // quariesButton
            // 
            this.quariesButton.Location = new System.Drawing.Point(8, 119);
            this.quariesButton.Name = "quariesButton";
            this.quariesButton.Size = new System.Drawing.Size(361, 48);
            this.quariesButton.TabIndex = 3;
            this.quariesButton.Text = "Запросы";
            this.quariesButton.UseVisualStyleBackColor = true;
            this.quariesButton.Click += new System.EventHandler(this.quariesButton_Click);
            // 
            // editTablesButton
            // 
            this.editTablesButton.Location = new System.Drawing.Point(8, 11);
            this.editTablesButton.Name = "editTablesButton";
            this.editTablesButton.Size = new System.Drawing.Size(361, 48);
            this.editTablesButton.TabIndex = 2;
            this.editTablesButton.Text = "Просмотр и редактирование таблиц";
            this.editTablesButton.UseVisualStyleBackColor = true;
            this.editTablesButton.Click += new System.EventHandler(this.editTablesButton_Click);
            // 
            // disconnectButton
            // 
            this.disconnectButton.Location = new System.Drawing.Point(12, 384);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(382, 43);
            this.disconnectButton.TabIndex = 2;
            this.disconnectButton.Text = "Отключиться";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // specialFunctionsButton
            // 
            this.specialFunctionsButton.Location = new System.Drawing.Point(8, 65);
            this.specialFunctionsButton.Name = "specialFunctionsButton";
            this.specialFunctionsButton.Size = new System.Drawing.Size(361, 48);
            this.specialFunctionsButton.TabIndex = 4;
            this.specialFunctionsButton.Text = "Специальные функции";
            this.specialFunctionsButton.UseVisualStyleBackColor = true;
            this.specialFunctionsButton.Click += new System.EventHandler(this.specialFunctionsButton_Click);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 439);
            this.ControlBox = false;
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainMenuForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главное меню";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainMenu_FormClosed);
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button quariesButton;
        private System.Windows.Forms.Button editTablesButton;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Button specialFunctionsButton;
    }
}