﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.View;

namespace TrainDataBase
{
    public partial class TableListForm : Form
    {
        OracleConnection connection;
        MainForm mainForm;

        string[] tableNames = new string[] { "Работники",
            "Начальники",
            "Диспетчеры",
            "Водители",
            "Мед осмотры водителей",
            "Бригады",
            "Локомотивы",
            "История проверки локомотивов",
            "История ремонта локомотивов",
            "Типы поездов",
            "Пассажиры",
            "Отделы",
            "Типы маршрутов",
            "Станции",
            "Расписание маршрутов",
            "Маршруты",
            "Поезда",
            "Расписание станции",
            "Билеты",
            "История задержек рейсов",
            "Типы причин задержки рейсов"};

        public TableListForm(MainForm mainForm, OracleConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
            this.mainForm = mainForm;
        }

        private void TableListForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void TableListForm_Load(object sender, EventArgs e)
        {
            tableListBox.Items.AddRange(tableNames);
        }

        private void showTable(Table table)
        {
            TableForm tb = new TableForm(mainForm, connection, table);
            tb.MdiParent = mainForm;
            tb.Show();
            this.Close();
        }

        private void tableListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(tableListBox.SelectedIndex)
            {
                case (int)TableId.Worker:
                    showTable(new Worker());
                    break;
                case (int)TableId.Manager:
                    showTable(new Manager());
                    break;
                case (int)TableId.Dispatcher:
                    showTable(new Dispatcher());
                    break;
                case (int)TableId.Driver:
                    showTable(new Driver());
                    break;
                case (int)TableId.Body_check_history:
                    showTable(new Body_check_history());
                    break;
                case (int)TableId.Brigade:
                    showTable(new Brigade());
                    break;
                case (int)TableId.Locomotive:
                    showTable(new Locomotive());
                    break;
                case (int)TableId.Locomotive_check_history:
                    showTable(new Locomotive_check_history());
                    break;
                case (int)TableId.Locomotive_repair_history:
                    showTable(new Locomotive_repair_history());
                    break;
                case (int)TableId.Train_type:
                    showTable(new Train_type());
                    break;
                case (int)TableId.Passenger:
                    showTable(new Passenger());
                    break;
                case (int)TableId.Department:
                    showTable(new Department());
                    break;
                case (int)TableId.Route_types:
                    showTable(new Route_types());
                    break;
                case (int)TableId.Railway_station:
                    showTable(new Railway_station());
                    break;
                case (int)TableId.Route_timetable:
                    showTable(new Route_timetable());
                    break;
                case (int)TableId.Route:
                    showTable(new Route());
                    break;
                case (int)TableId.Train:
                    showTable(new Train());
                    break;
                case (int)TableId.Station_timetable:
                    showTable(new Station_timetable());
                    break;
                case (int)TableId.Ticket:
                    showTable(new Ticket());
                    break;
                case (int)TableId.Delayed_trip_history:
                    showTable(new Delayed_trip_history());
                    break;
                case (int)TableId.Delay_reason_type:
                    showTable(new Delay_reason_type());
                    break;
                default:
                    break;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            MainMenuForm menu = new MainMenuForm(mainForm, connection);
            this.Close();
            menu.MdiParent = mainForm;
            menu.Show();
        }
    }
}
