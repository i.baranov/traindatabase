﻿namespace TrainDataBase.View
{
    partial class QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableView = new System.Windows.Forms.DataGridView();
            this.queryLabel = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.paramsPanel = new System.Windows.Forms.Panel();
            this.filtrButton = new System.Windows.Forms.Button();
            this.overallLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.tableView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableView
            // 
            this.tableView.AllowUserToAddRows = false;
            this.tableView.AllowUserToDeleteRows = false;
            this.tableView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableView.BackgroundColor = System.Drawing.Color.White;
            this.tableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableView.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.tableView.Location = new System.Drawing.Point(12, 81);
            this.tableView.Name = "tableView";
            this.tableView.ReadOnly = true;
            this.tableView.RowTemplate.Height = 28;
            this.tableView.Size = new System.Drawing.Size(739, 428);
            this.tableView.TabIndex = 1;
            // 
            // queryLabel
            // 
            this.queryLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.queryLabel.AutoSize = true;
            this.queryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.queryLabel.Location = new System.Drawing.Point(12, 9);
            this.queryLabel.Name = "queryLabel";
            this.queryLabel.Size = new System.Drawing.Size(154, 46);
            this.queryLabel.TabIndex = 4;
            this.queryLabel.Text = "Запрос";
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.backButton.Location = new System.Drawing.Point(12, 515);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(139, 45);
            this.backButton.TabIndex = 5;
            this.backButton.Text = "Назад";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // paramsPanel
            // 
            this.paramsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramsPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.paramsPanel.Location = new System.Drawing.Point(757, 78);
            this.paramsPanel.Name = "paramsPanel";
            this.paramsPanel.Size = new System.Drawing.Size(214, 435);
            this.paramsPanel.TabIndex = 6;
            // 
            // filtrButton
            // 
            this.filtrButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.filtrButton.Location = new System.Drawing.Point(757, 519);
            this.filtrButton.Name = "filtrButton";
            this.filtrButton.Size = new System.Drawing.Size(214, 45);
            this.filtrButton.TabIndex = 7;
            this.filtrButton.Text = "Фильтр";
            this.filtrButton.UseVisualStyleBackColor = true;
            this.filtrButton.Click += new System.EventHandler(this.filtrButton_Click);
            // 
            // overallLabel
            // 
            this.overallLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.overallLabel.AutoSize = true;
            this.overallLabel.Location = new System.Drawing.Point(3, 12);
            this.overallLabel.Name = "overallLabel";
            this.overallLabel.Size = new System.Drawing.Size(127, 20);
            this.overallLabel.TabIndex = 8;
            this.overallLabel.Text = "Общее число: 0";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.overallLabel);
            this.panel1.Location = new System.Drawing.Point(157, 515);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(140, 42);
            this.panel1.TabIndex = 9;
            // 
            // QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 572);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.filtrButton);
            this.Controls.Add(this.paramsPanel);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.queryLabel);
            this.Controls.Add(this.tableView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(25, 25);
            this.Name = "QueryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "QueryForm";
            this.Load += new System.EventHandler(this.QueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView tableView;
        private System.Windows.Forms.Label queryLabel;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Panel paramsPanel;
        private System.Windows.Forms.Button filtrButton;
        private System.Windows.Forms.Label overallLabel;
        private System.Windows.Forms.Panel panel1;
    }
}