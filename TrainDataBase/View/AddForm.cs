﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;
using TrainDataBase.View;

namespace TrainDataBase.AddForms
{
    public partial class AddForm : Form
    {
        MainForm mainForm;
        OracleConnection connection;
        List<Control> controls = new List<Control>();
        List<DbTypes> types = new List<DbTypes>();
        Table table;
        int id = 0;

        public AddForm(MainForm mainForm, List<Field> fields, OracleConnection connection, Table table)
        {
            InitializeComponent();
            this.connection = connection;
            this.table = table;
            this.mainForm = mainForm;
            addFieldsToPanel(fields);
        }

        private void addFieldsToPanel(List<Field> fields)
        {
            foreach (Field field in fields)
            {
                addOneField(field,id);
                id+=2;
            }
        }

        private void addFieldLabel(Field field, int id)
        {
            Label fieldName = new Label();
            fieldName.Location = new System.Drawing.Point(10, 25 * id + 5);
            fieldName.Name = "lB" + (id).ToString();
            fieldName.Size = new System.Drawing.Size(300, 20);
            fieldName.Text = field.viewName;
            fieldsPanel.Controls.Add(fieldName);
        }

        private void addDropDownList(Field field, int id, string[] items)
        {
            ComboBox dropDown = new ComboBox();
            dropDown.Location = new Point(10, 25 * (id + 1) + 5);
            dropDown.Name = "cB" + (id + 1).ToString();
            dropDown.Size = new Size(300, 20);
            dropDown.Items.AddRange(items);
            dropDown.DropDownStyle = ComboBoxStyle.DropDownList;
            fieldsPanel.Controls.Add(dropDown);
            controls.Add(dropDown);
        }

        private void addDatePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(300, 20);
            //dateTimePicker.Format = DateTimePickerFormat.Long;
            dateTimePicker.CustomFormat = "dd-MM-yyyy";
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            fieldsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
        }

        private void addTextBox(Field field, int id)
        {
            TextBox fieldTextBox = new System.Windows.Forms.TextBox();
            fieldTextBox.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            fieldTextBox.Name = "tB" + (id + 1).ToString();
            fieldTextBox.Size = new System.Drawing.Size(300, 20);
            fieldTextBox.TabIndex = 19;
            fieldsPanel.Controls.Add(fieldTextBox);
            controls.Add(fieldTextBox);
        }

        private void addNumericUpDown(Field field, int id)
        {
            NumericUpDown numericUpDown = new NumericUpDown();
            numericUpDown.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            numericUpDown.Name = "nUD" + (id + 1).ToString();
            numericUpDown.Maximum = 1000000000;
            numericUpDown.Size = new System.Drawing.Size(300, 20);
            fieldsPanel.Controls.Add(numericUpDown);
            controls.Add(numericUpDown);
        }

        private void addTimePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(300, 20);
            //dateTimePicker.Format = DateTimePickerFormat.Long;
            dateTimePicker.CustomFormat = "yyyy-MM-dd hh:m"; //YYYY-MM-DD HH24:MI
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            fieldsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
        }

        private string[] getViewNames(Reference reference)
        {
            var keyValues = DBOracleUtils.SelectEntity(connection,
                        reference.tableName,
                        reference.refFieldName);

            List<string> items = new List<string>();
            for (int i=0; i<keyValues.Count; i++)
            {
                var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValues[i], reference.refFieldName, reference.viewFields);
                string viewName = "";
                for (int j=0; j < viewNames.Count; j++)
                {
                    if (j > 0) viewName += " ";
                    viewName += viewNames[j]; 
                }
                items.Add(viewName);
            }

            items = items.Distinct().ToList();
            return items.ToArray();
        }

        private void addOneField(Field field, int id)
        {
            switch(field.type)
            {
                case DbTypes.DbKey:
                    this.id -= 2;
                    types.Add(DbTypes.DbKey);
                    controls.Add(new Label());
                    break;
                case DbTypes.DbRefference:
                    addFieldLabel(field, id);
                    var items = getViewNames(field.reference);
                    addDropDownList(field, id, items);
                    types.Add(DbTypes.DbRefference);
                    break;
                case DbTypes.DbGender:
                    addFieldLabel(field, id);
                    addDropDownList(field, id, new string[] { "Муж.", "Жен." });
                    types.Add(DbTypes.DbGender);
                    break;
                case DbTypes.DbBool:
                    addFieldLabel(field, id);
                    addDropDownList(field, id, new string[] { "Да", "Нет" });
                    types.Add(DbTypes.DbBool);
                    break;
                case DbTypes.DbDate:
                    addFieldLabel(field, id);
                    addDatePicker(field, id);
                    types.Add(DbTypes.DbDate);
                    break;
                case DbTypes.DbTime:
                    addFieldLabel(field, id);
                    addTimePicker(field, id);
                    types.Add(DbTypes.DbTime);
                    break;
                case DbTypes.DbInt:
                    addFieldLabel(field, id);
                    addNumericUpDown(field, id);
                    types.Add(DbTypes.DbInt);
                    break;
                default:
                    addFieldLabel(field, id);
                    addTextBox(field, id);
                    types.Add(DbTypes.DbVarchar);
                    break;
            }
        }

        private void AddForm_Load(object sender, EventArgs e)
        {

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (table.name == "SpecialManager" || 
                table.name == "SpecialDriver" ||
                table.name == "SpecialDispacher" ||
                table.name == "SpecialInsertDelay")
            {
                this.Close();
                SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
                sffl.MdiParent = mainForm;
                sffl.Show();
                return;
            }

            this.Close();
            TableForm tableForm = new TableForm(mainForm, connection, table);
            tableForm.MdiParent = mainForm;
            tableForm.Show();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            string valuesString = "";
            //int i = 0;
          //  if (table.fields[0].type == DbTypes.DbKey)
          //  {
          //      valuesString = "0,";
           // }
            for (int i=0; i < table.fields.Count; i++)
            {
                if (i>0) valuesString += ",";
                switch (types[i])
                {
                    case DbTypes.DbKey:
                        valuesString += "0";
                        break;
                    case DbTypes.DbDate:
                        DateTimePicker dtp = (DateTimePicker)controls[i];
                        if (dtp.Checked)
                        {
                            valuesString += "'" + dtp.Text + "'";
                        }
                        else
                        {
                            valuesString += "NULL";
                        }
                        break;
                    case DbTypes.DbTime:
                        DateTimePicker dttp = (DateTimePicker)controls[i];
                        if (dttp.Checked)
                        {
                            valuesString += "TO_TIMESTAMP(" + "'" + dttp.Text + "'" + ",'YYYY-MM-DD HH24:MI')";
                        }
                        else
                        {
                            valuesString += "NULL";
                        }
                        break;
                    case DbTypes.DbBool:
                        valuesString += "'" + DbBool.getBool(controls[i].Text) + "'";
                        break;
                    case DbTypes.DbGender:
                        valuesString += "'" + DbGender.getBool(controls[i].Text) + "'";
                        break;
                    case DbTypes.DbInt:
                        valuesString += controls[i].Text;
                        break;
                    case DbTypes.DbRefference:
                        valuesString += DBOracleUtils.SelectKeyWithClause(
                            connection, table.fields[i].reference.tableName,
                            table.fields[i].reference.refFieldName,
                            table.fields[i].reference.viewFields,
                            controls[i].Text.Split(' '))[0];
                        break;
                    default:
                        valuesString += "'" + controls[i].Text + "'";
                        break;
                }
            }

            if (table.name == "SpecialManager")
            {
                if (!DBOracleUtils.InsertManager(connection,valuesString))
                {
                    if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                    {
                        return;
                    };
                }
                this.Close();
                SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
                sffl.MdiParent = mainForm;
                sffl.Show();
                return;
            }

            if (table.name == "SpecialDriver")
            {
                if (!DBOracleUtils.InsertDriver(connection, valuesString))
                {
                    if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                    {
                        return;
                    };
                }
                this.Close();
                SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
                sffl.MdiParent = mainForm;
                sffl.Show();
                return;
            }

            if (table.name == "SpecialDispacher")
            {

                if (!DBOracleUtils.InsertDispacher(connection, valuesString))
                {
                    if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                    {
                        return;
                    };
                }
                this.Close();
                SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
                sffl.MdiParent = mainForm;
                sffl.Show();
                return;
            }

            if (table.name == "SpecialInsertDelay")
            {
                if (!DBOracleUtils.InsertDelay(connection, valuesString))
                {
                    if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                    {
                        return;
                    };
                }
                this.Close();
                SpecialFunctionsFormList sffl = new SpecialFunctionsFormList(mainForm, connection);
                sffl.MdiParent = mainForm;
                sffl.Show();
                return;
            }

            if (!DBOracleUtils.InsertData(connection, table.name, valuesString))
            {
                if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                };
            };
            this.Close();
            TableForm tableForm = new TableForm(mainForm, connection, table);
            tableForm.MdiParent = mainForm;
            tableForm.Show();
        }

        private void fieldsPanel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
