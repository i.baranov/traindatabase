﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.View
{
    public partial class EditForm : Form
    {
        MainForm mainForm;
        OracleConnection connection;
        List<string> values;
        List<Control> controls = new List<Control>();
        List<DbTypes> types = new List<DbTypes>();
        Table table;
        int id = 0;

        public EditForm(MainForm mainForm, List<Field> fields, 
            List<string> values,
            OracleConnection connection,
            Table table)
        {
            InitializeComponent();
            this.table = table;
            this.connection = connection;
            this.values = values;
            this.mainForm = mainForm;
            addFieldsToPanel(fields,values);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
            TableForm tableForm = new TableForm(mainForm, connection, table);
            tableForm.MdiParent = mainForm;
            tableForm.Show();
        }

        private void addFieldsToPanel(List<Field> fields, List<string> values)
        {
            for (int i=0; i<fields.Count; i++)
            {
                addOneField(fields[i], values[i], id);
                id += 2;
            }
        }

        private Label addFieldLabel(Field field, int id)
        {
            Label fieldName = new Label();
            fieldName.Location = new System.Drawing.Point(10, 25 * id + 5);
            fieldName.Name = "lB" + (id).ToString();
            fieldName.Size = new System.Drawing.Size(300, 20);
            fieldName.Text = field.viewName;
            fieldsPanel.Controls.Add(fieldName);
            return fieldName;
        }

        private ComboBox addDropDownList(Field field, int id, string[] items)
        {
            ComboBox dropDown = new ComboBox();
            dropDown.Location = new Point(10, 25 * (id + 1) + 5);
            dropDown.Name = "cB" + (id + 1).ToString();
            dropDown.Size = new Size(300, 20);
            dropDown.Items.AddRange(items);
            dropDown.Enabled = field.editable;
            dropDown.DropDownStyle = ComboBoxStyle.DropDownList;
            fieldsPanel.Controls.Add(dropDown);
            controls.Add(dropDown);
            return dropDown;
        }

        private DateTimePicker addDatePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(300, 20);
            dateTimePicker.CustomFormat = "dd-MM-yyyy";
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            dateTimePicker.Enabled = field.editable;
            fieldsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
            return dateTimePicker;
        }

        private TextBox addTextBox(Field field, int id)
        {
            TextBox fieldTextBox = new System.Windows.Forms.TextBox();
            fieldTextBox.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            fieldTextBox.Name = "tB" + (id + 1).ToString();
            fieldTextBox.Size = new System.Drawing.Size(300, 20);
            fieldTextBox.Enabled = field.editable;
            fieldsPanel.Controls.Add(fieldTextBox);
            controls.Add(fieldTextBox);
            return fieldTextBox;
        }

        private NumericUpDown addNumericUpDown(Field field, int id)
        {
            NumericUpDown numericUpDown = new NumericUpDown();
            numericUpDown.Location = new System.Drawing.Point(10, 25 * (id + 1) + 5);
            numericUpDown.Name = "nUD" + (id + 1).ToString();
            numericUpDown.Size = new System.Drawing.Size(300, 20);
            numericUpDown.Maximum = 1000000000;
            fieldsPanel.Controls.Add(numericUpDown);
            numericUpDown.Enabled = field.editable;
            controls.Add(numericUpDown);
            return numericUpDown;
        }

        private string getViewName(Reference reference, string keyValue)
        {
            List<string> items = new List<string>();

            var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValue, reference.refFieldName, reference.viewFields);
            string viewName = "";
            for (int j = 0; j < viewNames.Count; j++)
            {
                if (j > 0) viewName += " ";
                viewName += viewNames[j];
            }

            return viewName;
        }

        private string[] getViewNames(Reference reference)
        {
            var keyValues = DBOracleUtils.SelectEntity(connection,
                        reference.tableName,
                        reference.refFieldName);

            List<string> items = new List<string>();
            for (int i = 0; i < keyValues.Count; i++)
            {
                var viewNames = DBOracleUtils.SelectViewNames(connection,
                reference.tableName, keyValues[i], reference.refFieldName, reference.viewFields);
                string viewName = "";
                for (int j = 0; j < viewNames.Count; j++)
                {
                    if (j > 0) viewName += " ";
                    viewName += viewNames[j];
                }
                items.Add(viewName);
            }

            items = items.Distinct().ToList();
            return items.ToArray();
        }

        private DateTimePicker addTimePicker(Field field, int id)
        {
            DateTimePicker dateTimePicker = new DateTimePicker();
            dateTimePicker.Location = new Point(10, 25 * (id + 1) + 5);
            dateTimePicker.Name = "dtP" + (id + 1).ToString();
            dateTimePicker.Size = new System.Drawing.Size(300, 20);
            //dateTimePicker.Format = DateTimePickerFormat.Long;
            dateTimePicker.CustomFormat = "yyyy-MM-dd hh:m"; //YYYY-MM-DD HH24:MI
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.ShowCheckBox = true;
            fieldsPanel.Controls.Add(dateTimePicker);
            controls.Add(dateTimePicker);
            return dateTimePicker;
        }

        private void addOneField(Field field, string value, int id)
        {
            switch (field.type)
            {
                case DbTypes.DbKey:
                    this.id -= 2;
                    types.Add(DbTypes.DbKey);
                    controls.Add(new Label());
                    break;
                case DbTypes.DbRefference:
                    addFieldLabel(field, id);
                    var items = getViewNames(field.reference);
                    var dbList = addDropDownList(field,
                        id,
                        items);
                    dbList.Text = getViewName(field.reference, value);
                    types.Add(DbTypes.DbRefference);
                    break;
                case DbTypes.DbGender:
                    addFieldLabel(field, id);
                    var dblist = 
                        addDropDownList(field, id, new string[] { "Муж.", "Жен." });
                    dblist.Text = value;
                    types.Add(DbTypes.DbGender);
                    break;
                case DbTypes.DbTime:
                    addFieldLabel(field, id);
                    var tpicker = 
                        addTimePicker(field, id);
                    tpicker.Text = value;
                    types.Add(DbTypes.DbTime);
                    break;
                case DbTypes.DbDate:
                    addFieldLabel(field, id);
                    var dataPicker = addDatePicker(field, id);
                    try
                    {
                        dataPicker.Text = value;
                    }
                    catch
                    {
                        dataPicker.Checked = false;
                    }
                    types.Add(DbTypes.DbDate);
                    break;
                case DbTypes.DbBool:
                    addFieldLabel(field, id);
                    var dblistBool = addDropDownList(field, id, new string[] { "Да", "Нет" });
                    dblistBool.Text = value;
                    types.Add(DbTypes.DbBool);
                    break;
                case DbTypes.DbInt:
                    addFieldLabel(field, id);
                    var numericUpDown = addNumericUpDown(field, id);
                    numericUpDown.Value = Convert.ToDecimal(value);
                    types.Add(DbTypes.DbInt);
                    break;
                default:
                    addFieldLabel(field, id);
                    var textBox = addTextBox(field, id);
                    textBox.Text = value;
                    types.Add(DbTypes.DbVarchar);
                    break;
            }
        }

        private void EditForm_Load(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string valuesString = "";
            string key = "";
            string keyName = "";

            bool keyAppers = types.Contains(DbTypes.DbKey);

            for (int i = 0; i < controls.Count; i++)
            {
                if (i != 0 && table.fields[i].type != DbTypes.DbKey && valuesString != "")
                {
                    valuesString += ",";
                }

                if (table.fields[i].type != DbTypes.DbKey)
                {
                    valuesString += table.fields[i].name + "=";
                }
                switch (types[i])
                {
                    case DbTypes.DbKey:
                        key = values[i];
                        keyName = table.fields[i].name;
                        break;
                    case DbTypes.DbDate:
                        DateTimePicker dtp = (DateTimePicker)controls[i];
                        if (dtp.Checked)
                        {
                            valuesString += "'" + dtp.Text + "'";
                        }
                        else
                        {
                            valuesString += "NULL";
                        }
                        break;
                    case DbTypes.DbTime:
                        DateTimePicker dttp = (DateTimePicker)controls[i];
                        if (dttp.Checked)
                        {
                            valuesString += "TO_TIMESTAMP(" + "'" + dttp.Text + "'" + ",'YYYY-MM-DD HH24:MI')";
                        }
                        else
                        {
                            valuesString += "NULL";
                        }
                        break;
                    case DbTypes.DbBool:
                        valuesString += "'" + DbBool.getBool(controls[i].Text) + "'";
                        break;
                    case DbTypes.DbGender:
                        valuesString += "'" + DbGender.getBool(controls[i].Text) + "'";
                        break;
                    case DbTypes.DbInt:
                        valuesString += controls[i].Text;
                        break;
                    case DbTypes.DbRefference:
                        if (!keyAppers)
                        {
                            key = values[i];
                            keyName = table.fields[i].name;
                        }
                        valuesString += DBOracleUtils.SelectKeyWithClause(
                            connection, table.fields[i].reference.tableName,
                            table.fields[i].reference.refFieldName,
                            table.fields[i].reference.viewFields,
                            controls[i].Text.Split(' '))[0];
                        break;
                    default:
                        valuesString += "'" + controls[i].Text + "'";
                        break;
                }

            }
            if (!DBOracleUtils.EditData(connection, table.name, valuesString, keyName, key))
            {
                if (MessageBox.Show("Запись не была добавлена. " +
                    "Проверьте правильность введенных данных. Повторить попытку?",
                    "Ошибка", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                };
            };
            this.Close();
            TableForm tableForm = new TableForm(mainForm, connection, table);
            tableForm.MdiParent = mainForm;
            tableForm.Show();
        }
    }
}
