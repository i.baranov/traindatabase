﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainDataBase.AddForms;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;

namespace TrainDataBase.View
{
    public partial class SpecialFunctionsFormList : Form
    {
        OracleConnection connection;
        MainForm mainForm;

        string[] queryNames = new string[] {
            "Добавить начальника",
            "Добавить водителя",
            "Добавить диспетчера",
            "Добавить задержку рейса"
        };

        public SpecialFunctionsFormList(MainForm mainForm, OracleConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
            this.mainForm = mainForm;
        }

        private void SpecialFunctionsFormList_Load(object sender, EventArgs e)
        {
            tableListBox.Items.AddRange(queryNames);
        }

        private void tableListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tableListBox.SelectedIndex)
            {
                case 0:
                    SpecialManager specialManager = new SpecialManager();
                    AddForm addForm = new AddForm(mainForm, specialManager.fields, connection, specialManager);
                    addForm.MdiParent = mainForm;
                    addForm.Show();
                    this.Close();
                    break;
                case 1:
                    SpecialDriver specialDriver = new SpecialDriver();
                    AddForm addForm2 = new AddForm(mainForm, specialDriver.fields, connection, specialDriver);
                    addForm2.MdiParent = mainForm;
                    addForm2.Show();
                    this.Close();
                    break;
                case 2:
                    SpecialDispacher specialDispacher = new SpecialDispacher();
                    AddForm addForm3 = new AddForm(mainForm, specialDispacher.fields, connection, specialDispacher);
                    addForm3.MdiParent = mainForm;
                    addForm3.Show();
                    this.Close();
                    break;
                case 3:
                    SpecialInsertDelay specialInsertDelay = new SpecialInsertDelay();
                    AddForm addForm4 = new AddForm(mainForm, specialInsertDelay.fields, connection, specialInsertDelay);
                    addForm4.MdiParent = mainForm;
                    addForm4.Show();
                    this.Close();
                    break;
                default:
                    break;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            MainMenuForm menu = new MainMenuForm(mainForm, connection);
            this.Close();
            menu.MdiParent = mainForm;
            menu.Show();
        }
    }
}
