﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class SpecialManager : Table
    {
        public SpecialManager()
        {
            Worker worker = new Worker();
            Manager manager = new Manager();
            
            worker.fields.Add(manager.fields[1]);
            this.fields = worker.fields;

            this.name = "SpecialManager";
        }
    }
}
