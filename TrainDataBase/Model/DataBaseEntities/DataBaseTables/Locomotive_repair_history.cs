﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Locomotive_repair_history : Table
    {
        public Locomotive_repair_history()
        {
            name = "Locomotive_repair_history";
            viewName = "История ремонта локомотивов";

            fields = new List<Field> {
                new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),
                new Field("Locomotive_id", DbTypes.DbRefference, "Идентификатор локомотива",
                true, new Reference("Locomotive", "Locomotive_id", new string[]{ "Locomotive_id" })),

                new Field("Repair_date", DbTypes.DbDate, "Дата проверки"),
                new Field("Repair_result", DbTypes.DbBool, "Успешная проверка?"),
            };
        }
    }
}
