﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Department : Table
    {
        public Department()
        {
            name = "Department";
            viewName = "Отделы";

            fields = new List<Field> {
                new Field("Department_id", DbTypes.DbKey, "Идентификатор отдела", false),

                new Field("Brigade_id", DbTypes.DbRefference,
                "Номер бригады", true,
                new Reference("Brigade", "Brigade_id", new string[] { "Brigade_number" })),

                new Field("Station_id", DbTypes.DbRefference,
                "Название станции", true,
                new Reference("Railway_station", "Station_id", new string[] { "Station_name" })),

                new Field("Department_number", DbTypes.DbInt, "Номер отдела"),

                new Field("Manager_id", DbTypes.DbRefference, "Фамилия,имя начальника", true,
                new Reference("Worker", "Worker_id", new string[]{ "First_name", "Last_name" }))
            };
        }
    }
}
