﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Passenger : Table
    {
        public Passenger()
        {
            name = "Passenger";
            viewName = "Пассажиры";

            fields = new List<Field> {
                new Field("Passenger_id", DbTypes.DbKey, "Идентификатор пассажира", false),
                new Field("First_name", DbTypes.DbVarchar, "Имя"),
                new Field("Last_name",DbTypes.DbVarchar, "Фамилия"),
                new Field("Birth_date", DbTypes.DbDate, "Дата рождения"),
                new Field("Gender", DbTypes.DbGender, "Пол")
            };
        }
    }
}
