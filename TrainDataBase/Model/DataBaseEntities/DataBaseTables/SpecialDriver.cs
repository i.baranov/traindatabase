﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class SpecialDriver : Table
    {
        public SpecialDriver()
        {
            Worker worker = new Worker();
            Driver driver = new Driver();

            worker.fields.Add(driver.fields[1]);
            this.fields = worker.fields;

            this.name = "SpecialDriver";
        }
    }
}
