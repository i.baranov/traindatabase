﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Route_timetable : Table
    {
        public Route_timetable()
        {
            name = "Route_timetable";
            viewName = "Расписание маршрутов";

            fields = new List<Field> {
                new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),

                new Field("Route_id", DbTypes.DbRefference, "Номер маршрута",
                true, new Reference("Route", "Route_id", new string[]{ "Train_number" })),

                new Field("Station_id", DbTypes.DbRefference,
                "Название станции", true,
                new Reference("Railway_station", "Station_id", new string[] { "Station_name" })),

                new Field("Time_from_start_station", DbTypes.DbInt, "Время от стартовой станции (мин)"),

                new Field("Stop_time", DbTypes.DbInt, "Время остановки")
            };
        }
    }
}
