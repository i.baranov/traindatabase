﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Railway_station : Table
    {
        public Railway_station()
        {
            name = "Railway_station";
            viewName = "Станции";

            fields = new List<Field> {
                new Field("Station_id",
                    DbTypes.DbKey,
                    "Идентификатор станции", false),

                new Field("Station_name", DbTypes.DbVarchar, "Название станции", true)
            };
        }
    }
}
