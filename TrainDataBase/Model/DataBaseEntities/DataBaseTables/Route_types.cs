﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Route_types : Table
    {
        public Route_types()
        {
            name = "Route_types";
            viewName = "Типы маршрутов";

            fields = new List<Field> {
                new Field("Route_type_id",
                    DbTypes.DbKey,
                    "Идентификатор типа", false),

                new Field("Route_type_name", DbTypes.DbVarchar, "Тип", true)
            };
        }
    }
}
