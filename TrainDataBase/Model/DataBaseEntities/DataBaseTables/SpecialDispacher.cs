﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class SpecialDispacher : Table
    {
        public SpecialDispacher()
        {
            Worker worker = new Worker();
            Dispatcher driver = new Dispatcher();

            worker.fields.Add(driver.fields[1]);
            this.fields = worker.fields;

            this.name = "SpecialDispacher";
        }
    }
}
