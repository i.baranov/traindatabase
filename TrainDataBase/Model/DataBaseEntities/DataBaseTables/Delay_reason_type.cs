﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Delay_reason_type : Table
    {
        public Delay_reason_type()
        {
            name = "Delay_reason_type";
            viewName = "Типы причин задержки рейсов";

            fields = new List<Field> {
                new Field("Reason_id",
                    DbTypes.DbKey,
                    "Идентификатор типа", false),

                new Field("Reason", DbTypes.DbVarchar, "Тип", true)
            };
        }
    }
}
