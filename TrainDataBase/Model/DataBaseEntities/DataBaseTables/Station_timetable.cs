﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Station_timetable : Table
    {
        public Station_timetable()
        {
            name = "Station_timetable";
            viewName = "Расписание станции";

            fields = new List<Field> {
                new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),

                new Field("Train_id", DbTypes.DbRefference, "Идентификатор поезда",
                true, new Reference("Train", "Train_id", new string[]{ "Train_id" })),

                new Field("Arrival_date", DbTypes.DbTime,
                "Время прибытия"),

                new Field("Route_id", DbTypes.DbRefference, "Номер маршрута",
                true, new Reference("Route", "Route_id", new string[]{ "Train_number" })),

                new Field("Delayed", DbTypes.DbBool, "Отложен ли рейс?"),

                new Field("Total_tickets", DbTypes.DbInt, "Всего билетов"),

                new Field("Canceled", DbTypes.DbBool, "Отменён ли рейс?")
            };
        }
    }
}
