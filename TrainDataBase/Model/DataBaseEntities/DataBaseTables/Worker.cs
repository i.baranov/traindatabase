﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.DataBaseEntities
{
    public class Worker : Table
    {
        public Worker()
        {
            name = "Worker";
            viewName = "Работники";

            fields = new List<Field> {
                new Field("Worker_id", DbTypes.DbKey, "Идентификатор работника", false),
                new Field("First_name", DbTypes.DbVarchar, "Имя"),
                new Field("Last_name",DbTypes.DbVarchar, "Фамилия"),
                new Field("Gender", DbTypes.DbGender, "Пол"),
                new Field("Birth_date", DbTypes.DbDate, "Дата рождения"),
                new Field("Children_count", DbTypes.DbInt, "Количество детей"),
                new Field("Salary", DbTypes.DbInt, "Зарплата"),
                new Field("Start_date", DbTypes.DbDate, "Дата начала работы"),
                new Field("End_date", DbTypes.DbDate, "Дата окончания работы")
            };
        }
    }
}
