﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Train : Table
    {
        public Train()
        {
            name = "Train";
            viewName = "Поезда";

            fields = new List<Field> {
                 new Field("Train_id",
                    DbTypes.DbKey,
                    "Идентификатор поезда", false),

                 new Field("Locomotive_id",
                    DbTypes.DbRefference,
                    "Идентификатор локомотива", true,
                    new Reference("Locomotive","Locomotive_id",
                    new string[] { "Locomotive_id" })),

                 new Field("Type_id",
                    DbTypes.DbRefference,
                    "Тип поезда", true,
                    new Reference("Train_type","Type_id",
                    new string[] { "Type_name" }))
            };
        }
    }
}
