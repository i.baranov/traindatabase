﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Locomotive_check_history : Table
    {
        public Locomotive_check_history()
        {
            name = "Locomotive_check_history";
            viewName = "История проверки локомотивов";

            fields = new List<Field> {
                new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),
                new Field("Locomotive_id", DbTypes.DbRefference, "Идентификатор локомотива",
                true, new Reference("Locomotive", "Locomotive_id", new string[]{ "Locomotive_id" })),

                new Field("Check_date", DbTypes.DbDate, "Дата проверки"),
                new Field("Check_result", DbTypes.DbBool, "Успешная проверка?"),
            };
        }
    }
}
