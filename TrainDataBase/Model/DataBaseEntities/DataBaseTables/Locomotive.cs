﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Locomotive : Table
    {
        public Locomotive()
        {
            name = "Locomotive";
            viewName = "Локомотивы";

            fields = new List<Field> {
                new Field("Locomotive_id",
                    DbTypes.DbKey,
                    "Идентификатор локомотива", false),

                new Field("Creation_date", DbTypes.DbDate, "Дата создания"),

                new Field("Brigade_id", DbTypes.DbRefference, 
                "Номер бригады", true, 
                new Reference("Brigade", "Brigade_id", new string[] { "Brigade_number" }))

            };
        }
    }
}
