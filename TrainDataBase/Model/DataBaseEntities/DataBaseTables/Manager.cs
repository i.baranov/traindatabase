﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities
{
    class Manager : Table
    {
        public Manager()
        {
            name = "Manager";
            viewName = "Начальники";

            fields = new List<Field> {
                new Field("Manager_id", 
                    DbTypes.DbRefference, 
                    "Фамилия,имя начальника", true, 
                    new Reference("Worker","Worker_id",
                    new string[] { "First_name", "Last_name" })),

                new Field("Manager_class", DbTypes.DbInt, "Класс начальника", true)
            };
        }
    }
}
