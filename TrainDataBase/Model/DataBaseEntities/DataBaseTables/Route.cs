﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Route : Table
    {
        public Route()
        {
            name = "Route";
            viewName = "Маршруты";

            fields = new List<Field> {
                 new Field("Route_id",
                    DbTypes.DbKey,
                    "Индекс маршрута", false),

                new Field("Route_type",
                    DbTypes.DbRefference,
                    "Тип маршрута", true,
                    new Reference("Route_types","Route_type_id",
                    new string[] { "Route_type_name" })),

                new Field("Train_number", DbTypes.DbInt, "Номер поезда", true)
            };
        }
    }
}
