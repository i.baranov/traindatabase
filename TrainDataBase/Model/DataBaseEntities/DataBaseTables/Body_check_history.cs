﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Body_check_history : Table
    {
        public Body_check_history()
        {
            name = "Body_check_history";
            viewName = "Мед осмотры водителей";

            fields = new List<Field> {
                new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),

                new Field("Body_check_date", DbTypes.DbDate, "Дата проверки"),
                new Field("Body_check_result", DbTypes.DbBool, "Успешная проверка?"),
                new Field("Driver_id", DbTypes.DbRefference, "Фамилия,имя водителя",
                true, new Reference("Worker", "Worker_id", new string[]{ "First_name", "Last_name" }))
            };
        }
    }
}
