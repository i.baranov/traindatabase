﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Delayed_trip_history : Table
    {
        public Delayed_trip_history()
        {
            name = "Delayed_trip_history";
            viewName = "История задержек рейсов";

            fields = new List<Field> {
                 new Field("Entry_id",
                    DbTypes.DbKey,
                    "Индекс записи", false),

                new Field("Station_timetable_entry",
                    DbTypes.DbRefference,
                    "Индекс рейса", true,
                    new Reference("Station_timetable","Entry_id",
                    new string[] { "Entry_id" })),

                new Field("Delay_reason_id",
                    DbTypes.DbRefference,
                    "Причина задержки", true,
                    new Reference("Delay_reason_type","Reason_id",
                    new string[] { "Reason" })),

                new Field("Delay_time",
                    DbTypes.DbInt,
                    "Время задержки (мин)", true)
            };
        }
    }
}
