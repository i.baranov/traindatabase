﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Train_type : Table
    {
        public Train_type()
        {
            name = "Train_type";
            viewName = "Типы поездов";

            fields = new List<Field> {
                new Field("Type_id",
                    DbTypes.DbKey,
                    "Идентификатор типа", false),

                new Field("Type_name", DbTypes.DbVarchar, "Тип", true)
            };
        }
    }
}
