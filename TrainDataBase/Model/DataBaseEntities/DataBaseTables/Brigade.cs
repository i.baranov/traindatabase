﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Brigade : Table
    {
        public Brigade()
        {
            name = "Brigade";
            viewName = "Бригады";

            fields = new List<Field> {
                new Field("Worker_id",
                    DbTypes.DbRefference,
                    "Фамилия,имя работника", true,
                    new Reference("Worker","Worker_id",
                    new string[] { "First_name", "Last_name" })),

                new Field("Brigade_id", DbTypes.DbKey, "Идентификатор бригады", false),

                new Field("Brigade_number", DbTypes.DbInt, "Номер бригады")
            };
        }
    }
}
