﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities
{
    class Dispatcher : Table
    {
        public Dispatcher()
        {
            name = "Dispatcher";
            viewName = "Диспетчеры";

            fields = new List<Field> {
                new Field("Dispatcher_Id",
                    DbTypes.DbRefference,
                    "Идентификатор диспетчера", true,
                    new Reference("Worker","Worker_id",
                    new string[] { "First_name", "Last_name" })),

                new Field("Main_dispacher", DbTypes.DbBool, "Главный диспетчер", true)
            };
        }
    }
}
