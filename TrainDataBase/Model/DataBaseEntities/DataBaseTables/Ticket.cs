﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class Ticket : Table
    {
        public Ticket()
        {
            name = "Ticket";
            viewName = "Билеты";

            fields = new List<Field> {
                new Field("Ticket_id", DbTypes.DbKey, "Идентификатор билета", false),
                new Field("Passenger_id", DbTypes.DbRefference, "Идентификатор пассажира",
                true, new Reference("Passenger","Passenger_id",
                    new string[] { "First_name", "Last_name" })),
                new Field("Time_table_entry", DbTypes.DbRefference, "Идентификатор рейса, маршрута",
                true, new Reference("Station_timetable","Entry_id",
                    new string[] { "Entry_id", "Route_id" })),
                new Field("Returned",DbTypes.DbBool, "Возвращен"),
                new Field("Booked", DbTypes.DbBool, "Забронирован"),
                new Field("Baggage", DbTypes.DbBool, "Багаж"),
                new Field("Ticket_price", DbTypes.DbInt, "Цена билета", false), 
                new Field("Start_station_id", DbTypes.DbRefference, "Начальная станция",
                true, new Reference("Railway_station","Station_id",
                    new string[] { "Station_name" })),
                new Field("End_station_id", DbTypes.DbRefference, "Конечная станция",
                true, new Reference("Railway_station","Station_id",
                    new string[] { "Station_name" })),

            };
        }
    }
}
