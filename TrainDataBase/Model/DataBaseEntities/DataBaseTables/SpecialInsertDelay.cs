﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseTables
{
    class SpecialInsertDelay : Table
    {
        public SpecialInsertDelay()
        {
            Delayed_trip_history delayed_trip_history = new Delayed_trip_history();
            
            this.fields = delayed_trip_history.fields;

            this.name = "SpecialInsertDelay";
        }
    }
}
