﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.Model.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.DataBaseEntities
{
    public class Field
    {
        public string name;
        public DbTypes type;
        public string viewName;
        public bool editable;
        public Reference reference;

        public Field()
        {
            this.name = null;
            this.type = DbTypes.DbInt;
            this.viewName = null;
            this.editable = true;
            this.reference = null;
        }

        public Field(string name, DbTypes type, string viewName, bool editable, Reference refference)
        {
            this.name = name;
            this.type = type;
            this.viewName = viewName;
            this.editable = editable;
            this.reference = refference;
        }

        public Field(string name, DbTypes type, string viewName,bool editable)
        {
            this.name = name;
            this.type = type;
            this.viewName = viewName;
            this.editable = editable;
            this.reference = null;
        }

        public Field(string name, DbTypes type, string viewName)
        {
            this.name = name;
            this.type = type;
            this.viewName = viewName;
            this.editable = true;
            this.reference = null;
        }

        public Field(Field field)
        {
            this.name = field.name;
            this.type = field.type;
            this.viewName = field.viewName;
            this.reference = field.reference;
            this.editable = field.editable;
        }
        
    }
}
