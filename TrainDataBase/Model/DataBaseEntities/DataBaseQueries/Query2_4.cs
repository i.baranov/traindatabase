﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query2_4 : Query
    {
        public Query2_4()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники указанной бригады указанного возраста";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Brigade", "Brigade_id", DbTypes.DbRefference,
                "Номер бригады", new Reference("Brigade", "Brigade_id", new string[] { "Brigade_number" }
                    )),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Возраст от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "1", "0", "100"
            };

            this.paramsRefs = new string[] { "?", "??", "???"};
            this.queryString = "(" + "WITH Age_Worker AS ( SELECT Worker_id, Round((months_between(CURRENT_DATE,Birth_date))/12,0) as Age FROM Worker ) SELECT * FROM Worker LEFT JOIN Age_Worker Using (Worker_id) WHERE Worker_id IN ( SELECT Worker_id FROM Brigade WHERE (BRIGADE_NUMBER = ?) ) AND (Age > ??) AND (Age < ???) " + ")";
        }
    }
}
