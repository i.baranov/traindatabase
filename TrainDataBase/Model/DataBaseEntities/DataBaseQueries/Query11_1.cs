﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query11_1 : Query
    {
        public Query11_1()
        {
            var passenger = new Passenger();
            this.name = passenger.name;
            this.fields = passenger.fields;

            this.viewName = "Пассажиры на указанном рейсе";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Station_timetable", "Entry_id", DbTypes.DbRefference,
               "Рейс", new Reference("Station_timetable", "Entry_id", new string[]{"Entry_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Passenger.* FROM Ticket JOIN Passenger ON Ticket.Passenger_id = Passenger.Passenger_id JOIN Station_timetable ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (Route_id = ?) AND(Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
