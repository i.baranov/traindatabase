﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_2 : Query
    {
        public Query1_2()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники указанного пола";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Gender", DbTypes.DbGender, "Пол")
            };
            this.defaultValues = new List<string>()
            {
                "Муж."
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM WORKER WHERE (Gender = ?)" + ")";
        }
    }
}
