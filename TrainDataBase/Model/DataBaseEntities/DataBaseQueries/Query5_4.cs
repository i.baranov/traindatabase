﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query5_4 : Query
    {
        public Query5_4()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;

            this.viewName = "Локомотивы по возрасту";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Возраст от"),
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "100"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "WITH Age_Locomotive AS ( SELECT Brigade_id, Creation_date, Locomotive_id, Round((months_between(CURRENT_DATE,Creation_date))/12,0) as Age FROM Locomotive ), Needed_Locomotives AS ( SELECT Locomotive_id, Creation_date, Brigade_id FROM Age_Locomotive WHERE (Age > ?) AND (AGE < ??) ) SELECT Locomotive_id, Creation_date, Brigade_id FROM Needed_Locomotives";
            this.queryString = "(" + query + ")";
        }
    }
}
