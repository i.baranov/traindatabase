﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query4_2 : Query
    {
        public Query4_2()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;
            fields[0].viewName = "Идентификатор локомотива";
            fields[1].viewName = "Время прибытия";
            fields[2].viewName = "Станция";
            fields[2].name = "Railway_station";
            fields[2].reference = new Reference("Railway_station", "Station_id", new string[] { "Station_name" }); 

            this.viewName = "Локомотив по времени прибытия на станции";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Locomotive", "Locomotive_id", DbTypes.DbRefference, "Локомотив",
                new Reference("Locomotive", "Locomotive_id", new string[] { "Locomotive_id" }))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT Locomotive_id, to_char(Arrival_date + Time_from_start_station/24/60, 'DD-MON-YYYY HH24:MI') as Arrival_time, Station_id FROM Locomotive JOIN Train Using (Locomotive_id) JOIN Station_timetable USING(Train_id) JOIN Route_timetable USING(Route_id) WHERE (Locomotive_id = ?) " + ")";
        }
    }
}
