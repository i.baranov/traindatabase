﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query8_1 : Query
    {
        public Query8_1()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = station_timetable.fields;

            this.viewName = "Задержанные рейсы полностью";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbEmpty,
               "Маршрут")
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT * FROM Delayed_trips";
            this.queryString = "(" + query + ")";
        }
    }
}
