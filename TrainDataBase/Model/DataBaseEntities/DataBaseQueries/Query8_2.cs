﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query8_2 : Query
    {
        public Query8_2()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = station_timetable.fields;

            this.viewName = "Задержанные рейсы по указанной причине";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Delay_reason_type", "Reason_id", DbTypes.DbRefference,
               "Причина", new Reference("Delay_reason_type", "Reason_id", new string[]{"Reason"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT * FROM Delayed_trips JOIN Delayed_trip_history ON Delayed_trips.Entry_id = Delayed_trip_history.Station_timetable_entry WHERE Delay_reason_id = ?";
            this.queryString = "(" + query + ")";
        }
    }
}
