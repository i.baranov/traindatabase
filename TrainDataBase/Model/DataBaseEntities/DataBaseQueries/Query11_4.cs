﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query11_4 : Query
    {
        public Query11_4()
        {
            var passenger = new Passenger();
            this.name = passenger.name;
            this.fields = passenger.fields;

            this.viewName = "Пассажиры по признаку сдачи в багажное отделение";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbBool, "Сдали багаж?")
            };
            this.defaultValues = new List<string>()
            {
                "Да"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Passenger.* FROM Ticket JOIN Passenger ON Ticket.Passenger_id = Passenger.Passenger_id WHERE (Baggage = ?) AND (Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
