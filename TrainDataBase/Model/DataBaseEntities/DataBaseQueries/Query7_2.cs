﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query7_2 : Query
    {
        public Query7_2()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = station_timetable.fields;

            this.viewName = "Отменённые рейсы в указанном направлении";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Railway_station", "Station_id", DbTypes.DbRefference,
               "Станция", new Reference("Railway_station", "Station_id", new string[]{"Station_name"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Canceled_trips.Entry_id, Train_id, Arrival_date, Route_id, Delayed, Total_tickets, Canceled FROM Canceled_trips JOIN Route_timetable USING (Route_id) WHERE Station_id = ? ";
            this.queryString = "(" + query + ")";
        }
    }
}
