﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query13_3 : Query
    {
        public Query13_3()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]), new Field(station_timetable.fields[1]) };
            this.fields[0].viewName = "Идентификатор маршрута";
            this.fields[1].viewName = "Общее число сданных билетов";

            this.viewName = "Общее число сданных билетов на указанный маршрут";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbRefference,
               "Маршрут", new Reference("Route", "Route_id", new string[]{"Route_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Route_id, COUNT(Entry_id) as Count FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (Route_id = ?) AND (Returned = 'Y') GROUP BY Route_id ";
            this.queryString = "(" + query + ")";
        }
    }
}
