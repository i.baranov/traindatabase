﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query11_5 : Query
    {
        public Query11_5()
        {
            var passenger = new Passenger();
            this.name = passenger.name;
            this.fields = passenger.fields;

            this.viewName = "Пассажиры по половому признаку";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Gender", DbTypes.DbGender,
               "Пол")
            };
            this.defaultValues = new List<string>()
            {
                "Муж."
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Passenger.* FROM Ticket JOIN Passenger ON Ticket.Passenger_id = Passenger.Passenger_id WHERE (Gender = ?) AND (Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
