﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_6 : Query
    {
        public Query1_6()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники по зарплате";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Зарплата от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "100000"
            };

            this.paramsRefs = new string[] { "?", "??" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE (Salary >= ?) AND (Salary <= ??)" + ")";
        }
    }
}
