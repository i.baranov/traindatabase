﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query4_1 : Query
    {
        public Query4_1()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;
            this.viewName = "Локомотивы, находящиеся на станции в указанное время";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Station_timetable", "Arrival_date", DbTypes.DbTime, "Время c"),
                new QueryParametr("Station_timetable", "Arrival_date", DbTypes.DbTime, "до")
            };
            this.defaultValues = new List<string>()
            {
                "", ""
            };

            this.paramsRefs = new string[] { "?", "??" };
            this.queryString = "(" + "SELECT DISTINCT Locomotive_id,Creation_date,Brigade_id FROM Locomotive JOIN Train Using (Locomotive_id) JOIN Station_timetable USING(Train_id) WHERE (Arrival_date > ?) AND (Arrival_date < ??)" + ")";
        }
    }
}
