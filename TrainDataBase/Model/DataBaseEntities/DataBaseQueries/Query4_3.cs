﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query4_3 : Query
    {
        public Query4_3()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = new List<Field>()
            {
                new Field("Locomotive", DbTypes.DbInt, "Количество совершённых маршрутов")
            };

            this.viewName = "Локомотив по количеству совершённых маршрутов";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Locomotive", "Locomotive_id", DbTypes.DbRefference, "Локомотив",
                new Reference("Locomotive", "Locomotive_id", new string[] { "Locomotive_id" }))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT COUNT(Entry_id) as COUNT FROM Station_timetable WHERE ( Train_id = ( SELECT Train_id FROM Locomotive JOIN Train Using (Locomotive_id) WHERE (Locomotive_id = ?) ) )" + ")";
        }
    }
}
