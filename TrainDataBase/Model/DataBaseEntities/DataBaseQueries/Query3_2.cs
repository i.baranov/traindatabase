﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query3_2 : Query
    {
        public Query3_2()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Водители указанного пола";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Gender", DbTypes.DbGender, "Пол")
            };
            this.defaultValues = new List<string>()
            {
                "Муж."
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE (Worker_id IN (SELECT Driver_id FROM Driver)) AND (Gender = ?)" + ")";
        }
    }
}
