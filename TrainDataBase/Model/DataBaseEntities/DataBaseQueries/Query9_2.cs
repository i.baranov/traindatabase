﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_2 : Query
    {
        public Query9_2()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]) };
            this.fields[0].viewName = "Среднее количество количество проданных билетов";

            this.viewName = "Среднее количество количество проданных билетов за указанный интервал времени";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Год от"),
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "2000", "2020"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "SELECT ROUND(AVG(Time_table_entry),2) as Average_count FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (extract(year From Arrival_date) > ?) AND (extract(year From Arrival_date) < ??) AND (Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
