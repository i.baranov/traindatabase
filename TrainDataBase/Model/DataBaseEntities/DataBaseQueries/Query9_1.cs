﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_1 : Query
    {
        public Query9_1()
        {
            var ticket = new Ticket();
            this.name = ticket.name;
            this.fields = ticket.fields;

            this.viewName = "Проданные билеты за указанный интервал времени";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Год от"),
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "2000", "2020"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "SELECT Ticket.* FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (extract(year From Arrival_date) > ?) AND (extract(year From Arrival_date) < ??) AND (Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
