﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_6 : Query
    {
        public Query9_6()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]) };
            this.fields[0].viewName = "Среднее количество количество проданных билетов";

            this.viewName = "Среднее количество количество проданных билетов по длительности маршрута";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Длительность")
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "WITH Ticket_count_of_stations AS ( SELECT Ticket_id, COUNT_STATIONS(Start_station_id, End_station_id, Route_id) as count, Time_table_entry FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id ) SELECT ROUND(AVG(Time_table_entry),2) as Average_count FROM Ticket_count_of_stations WHERE count = ?";
            this.queryString = "(" + query + ")";
        }
    }
}
