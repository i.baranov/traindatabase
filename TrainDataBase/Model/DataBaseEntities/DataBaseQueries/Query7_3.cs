﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query7_3 : Query
    {
        public Query7_3()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = station_timetable.fields;

            this.viewName = "Отменённые рейсы по указанному маршруту";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbRefference,
               "Маршрут", new Reference("Route", "Route_id", new string[]{"Route_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT DISTINCT Canceled_trips.Entry_id, Train_id, Arrival_date, Route_id, Delayed, Total_tickets, Canceled FROM Canceled_trips JOIN Route_timetable USING (Route_id) WHERE Route_id = ?";
            this.queryString = "(" + query + ")";
        }
    }
}
