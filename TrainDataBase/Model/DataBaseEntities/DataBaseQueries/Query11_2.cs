﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query11_2 : Query
    {
        public Query11_2()
        {
            var passenger = new Passenger();
            this.name = passenger.name;
            this.fields = passenger.fields;

            this.viewName = "Пассажиры, уехавшие в указанный день";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "День")
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Passenger.* FROM Ticket JOIN Passenger ON Ticket.Passenger_id = Passenger.Passenger_id JOIN Station_timetable ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (extract(day From Arrival_date) = ?) AND(Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
