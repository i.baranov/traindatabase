﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query6_1 : Query
    {
        public Query6_1()
        {
            var train = new Train();
            this.name = train.name;
            this.fields = train.fields;

            this.viewName = "Поезда на указанном маршруте";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbInt, "Маршрут",
               new Reference("Route", "Route_id", new string[] {"Route_id" }))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT * FROM Train JOIN Station_timetable USING (Train_id) WHERE (Route_id = ?)";
            this.queryString = "(" + query + ")";
        }
    }
}
