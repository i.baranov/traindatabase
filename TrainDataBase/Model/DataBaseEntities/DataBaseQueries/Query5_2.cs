﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query5_2 : Query
    {
        public Query5_2()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;

            this.viewName = "Локомотивы, отправленные в ремонт в указнное время";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Период (год) от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "До")
            };
            this.defaultValues = new List<string>()
            {
                "2000", "2021"
            };

            this.paramsRefs = new string[] { "?", "??" };
            this.queryString = "(" + "SELECT DISTINCT * FROM Locomotive JOIN Locomotive_repair_history Using (Locomotive_id) WHERE (extract(year From Repair_date) > ?) AND (extract(year From Repair_date) < ??)" + ")";
        }
    }
}
