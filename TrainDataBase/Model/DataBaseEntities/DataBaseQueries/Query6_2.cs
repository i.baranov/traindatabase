﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query6_2 : Query
    {
        public Query6_2()
        {
            var train = new Train();
            this.name = train.name;
            this.fields = train.fields;

            this.viewName = "Поезда по длительности маршрута";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Длительность (станций)")
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "WITH Train_route_duration AS ( SELECT Train_id, COUNT(Train_id) as route_duration FROM Train JOIN Station_timetable USING (Train_id) JOIN Route_timetable USING (Route_id) Group by (Train_id) ) SELECT * FROM Train JOIN Train_route_duration USING (Train_id) WHERE (route_duration = ?) ";
            this.queryString = "(" + query + ")";
        }
    }
}
