﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query10_2 : Query
    {
        public Query10_2()
        {
            var route = new Route();
            this.name = route.name;
            this.fields = route.fields;

            this.viewName = "Маршруты, следующие в определённом направлении";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Railway_station", "Station_id", DbTypes.DbRefference,
               "Станция", new Reference("Railway_station", "Station_id", new string[]{"Station_name"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT DISTINCT Route_id, Route_type, Train_number FROM Route JOIN Route_timetable USING (Route_id) WHERE (Station_id = ?)";
            this.queryString = "(" + query + ")";
        }
    }
}
