﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query5_3 : Query
    {
        public Query5_3()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;

            this.viewName = "Локомотивы, ремонтируемые указанное число раз";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Сколько раз?")
            };
            this.defaultValues = new List<string>()
            {
                "0"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "WITH Locomotive_repaircount AS ( SELECT Locomotive_id, COUNT(Entry_id) as repair_count FROM Locomotive JOIN Locomotive_repair_history Using (Locomotive_id) Group by(Locomotive_id) ) SELECT * FROM Locomotive JOIN Locomotive_repaircount USING (Locomotive_id) WHERE (repair_count = ?)" + ")";
        }
    }
}
