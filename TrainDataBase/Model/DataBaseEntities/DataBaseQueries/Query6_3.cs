﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query6_3 : Query
    {
        public Query6_3()
        {
            var train = new Train();
            this.name = train.name;
            this.fields = train.fields;

            this.viewName = "Поезда по цене билета";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Цена от"),
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "1000"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "SELECT DISTINCT Train_id, Locomotive_id, Type_id FROM Train JOIN Station_timetable USING (Train_id) JOIN Ticket ON (Time_table_entry = Entry_id) WHERE (Ticket_price > ?) AND (Ticket_price < ??)";
            this.queryString = "(" + query + ")";
        }
    }
}
