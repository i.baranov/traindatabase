﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query12_2 : Query
    {
        public Query12_2()
        {
            var ticket = new Ticket();
            this.name = ticket.name;
            this.fields = ticket.fields;

            this.viewName = "Невыкупленные билеты в указанный день";
            this.queryParams = new List<QueryParametr>()
            {
                 new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "День")
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Ticket.* FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (extract(day From Arrival_date) = ?) AND (Booked = 'Y')";
            this.queryString = "(" + query + ")";
        }
    }
}
