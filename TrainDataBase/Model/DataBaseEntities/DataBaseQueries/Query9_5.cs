﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_5 : Query
    {
        public Query9_5()
        {
            var ticket = new Ticket();
            this.name = ticket.name;
            this.fields = new List<Field>()
            {
                new Field("Ticket_id", DbTypes.DbInt, "Идентификатор билета"),
                new Field("Ticket_id", DbTypes.DbInt, "Длительность")
            };

            this.viewName = "Проданные билеты по длительности маршрута";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Длительность"),
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "WITH Ticket_count_of_stations AS ( SELECT Ticket_id, COUNT_STATIONS(Start_station_id, End_station_id, Route_id) as count FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id ) SELECT * FROM Ticket_count_of_stations WHERE count = ?";
            this.queryString = "(" + query + ")";
        }
    }
}
