﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_5 : Query
    {
        public Query1_5()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники по признаку наличия детей";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Gender", DbTypes.DbComparison, "Есть дети?")
            };
            this.defaultValues = new List<string>()
            {
                "Нет"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE (Children_count ? 0)" + ")";
        }
    }
}
