﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_7 : Query
    {
        public Query9_7()
        {
            var ticket = new Ticket();
            this.name = ticket.name;
            this.fields = ticket.fields;

            this.viewName = "Проданные билеты по цене билета";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Цена от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "300"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "SELECT * FROM Ticket WHERE (Ticket_price > ?) AND (Ticket_price < ??)";
            this.queryString = "(" + query + ")";
        }
    }
}
