﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query8_4 : Query
    {
        public Query8_4()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]) };
            this.fields[0].viewName = "Количество сданных билетов";

            this.viewName = "Количество сданных билетов во время всех задержек";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbEmpty,
               "Маршрут", new Reference("Route", "Route_id", new string[]{"Route_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "WITH Returned_tickets AS ( SELECT * FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE Returned = 'Y' ) SELECT COUNT(Entry_id) as Returned_tickets_count FROM Returned_tickets WHERE (Delayed = 'Y')";
            this.queryString = "(" + query + ")";
        }
    }
}
