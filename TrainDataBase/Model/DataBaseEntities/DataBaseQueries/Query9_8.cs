﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_8 : Query
    {
        public Query9_8()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]) };
            this.fields[0].viewName = "Среднее количество количество проданных билетов";

            this.viewName = "Среднее количество количество проданных билетов по цене билета";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Цена от"),
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "300"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "SELECT ROUND(AVG(Time_table_entry),2) as Average_count FROM Ticket WHERE (Ticket_price > ?) AND (Ticket_price < ??)";
            this.queryString = "(" + query + ")";
        }
    }
}
