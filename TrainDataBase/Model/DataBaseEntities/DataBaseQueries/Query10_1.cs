﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query10_1 : Query
    {
        public Query10_1()
        {
            var route = new Route();
            this.name = route.name;
            this.fields = route.fields;

            this.viewName = "Маршруты указанной категории";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route_types", "Route_type_id", DbTypes.DbRefference,
               "Категория", new Reference("Route_types", "Route_type_id", new string[]{"Route_type_name"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT * FROM Route WHERE Route_type = ?";
            this.queryString = "(" + query + ")";
        }
    }
}
