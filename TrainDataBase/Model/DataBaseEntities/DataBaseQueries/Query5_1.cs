﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query5_1 : Query
    {
        public Query5_1()
        {
            var locomotive = new Locomotive();
            this.name = locomotive.name;
            this.fields = locomotive.fields;

            this.viewName = "Локомотивы , прошедшие плановый техосмотр за период";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Период (год) от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "До")
            };
            this.defaultValues = new List<string>()
            {
                "2000", "2021"
            };

            this.paramsRefs = new string[] { "?", "??" };
            this.queryString = "(" + "SELECT * FROM Locomotive JOIN Locomotive_check_history Using (Locomotive_id) WHERE (extract(year From Check_date) > ?) AND (extract(year From Check_date) < ??) AND (Check_result = 'Y')" + ")";
        }
    }
}
