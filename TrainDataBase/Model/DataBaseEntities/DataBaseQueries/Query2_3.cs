﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query2_3 : Query
    {
        public Query2_3()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники, обслуживающие указанный локомотив";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Locomotive", "Locomotive_id", DbTypes.DbRefference,
                "Локомотив", new Reference("Locomotive", "Locomotive_id", new string[] { "Locomotive_id" }
                    ))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE Worker_id IN ( SELECT Worker_id FROM Brigade WHERE BRIGADE_NUMBER IN ( SELECT BRIGADE_NUMBER FROM ( SELECT * FROM Locomotive JOIN Brigade USING (Brigade_id) WHERE Locomotive_id = ?) ) )" + ")";
        }
    }
}
