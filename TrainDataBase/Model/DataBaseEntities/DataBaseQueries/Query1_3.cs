﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_3 : Query
    {
        public Query1_3()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники указанного возраста";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Возраст от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "100"
            };

            this.paramsRefs = new string[] { "?", "??"};
            this.queryString = "(" + "WITH Age_Worker AS ( SELECT Worker_id as Worker, Round((months_between(CURRENT_DATE,Birth_date))/12,0) as Age FROM Worker ), Needed_workers AS ( SELECT Worker FROM Age_Worker WHERE (Age > ?) AND (AGE < ??) ) SELECT * FROM Worker WHERE (Worker_id IN (SELECT Worker from Needed_workers))" + ")";
        }
    }
}
