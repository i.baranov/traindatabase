﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_1 : Query
    {
        public Query1_1()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники жележнодрожной станции указанного отдела";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Department", "Department_id", DbTypes.DbRefference,
                "Номер отдела", new Reference("Department", "Department_id", new string[] { "Department_number" })),
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE Worker_id IN ( SELECT Worker_id FROM Brigade WHERE BRIGADE_NUMBER IN ( SELECT BRIGADE_NUMBER FROM ( SELECT * FROM Department JOIN Brigade USING (Brigade_id) WHERE Department_number = ?) ) )" + ")";
        }
    }
}
