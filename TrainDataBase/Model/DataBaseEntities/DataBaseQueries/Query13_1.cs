﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query13_1 : Query
    {
        public Query13_1()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = new List<Field> { new Field(station_timetable.fields[0]), new Field(station_timetable.fields[1]) };
            this.fields[0].viewName = "Идентификатор рейса";
            this.fields[1].viewName = "Общее число сданных билетов";

            this.viewName = "Общее число сданных билетов на указанный рейс";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Station_timetable", "Entry_id", DbTypes.DbRefference,
               "Рейс", new Reference("Station_timetable", "Entry_id", new string[]{"Entry_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Entry_id, COUNT(Ticket_id) as Count FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE Entry_id = ? AND (Returned = 'Y') GROUP BY Entry_id, Total_tickets";
            this.queryString = "(" + query + ")";
        }
    }
}
