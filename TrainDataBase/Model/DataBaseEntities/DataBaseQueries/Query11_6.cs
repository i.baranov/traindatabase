﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query11_6 : Query
    {
        public Query11_6()
        {
            var passenger = new Passenger();
            this.name = passenger.name;
            this.fields = passenger.fields;

            this.viewName = "Пассажиры по возрасту";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Возраст от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "100"
            };

            this.paramsRefs = new string[] { "?", "??" };
            string query;
            query = "WITH Age_Passanger AS ( SELECT Passenger_id as Passanger, Round((months_between(CURRENT_DATE,Birth_date))/12,0) as Age FROM Passenger), Needed_passangers AS ( SELECT Passanger FROM Age_Passanger WHERE (Age > ?) AND (AGE < ??) ) SELECT * FROM Passenger WHERE (Passenger_id IN (SELECT Passanger from Needed_passangers))";
            this.queryString = "(" + query + ")";
        }
    }
}
