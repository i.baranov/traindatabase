﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query7_1 : Query
    {
        public Query7_1()
        {
            var station_timetable = new Station_timetable();
            this.name = station_timetable.name;
            this.fields = station_timetable.fields;

            this.viewName = "Отменённые рейсы";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr()
            };
            this.defaultValues = new List<string>()
            {
                "0"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT * FROM Canceled_trips";
            this.queryString = "(" + query + ")";
        }
    }
}
