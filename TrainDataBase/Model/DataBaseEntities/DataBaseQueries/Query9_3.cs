﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseEntities.DataBaseTables;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query9_3 : Query
    {
        public Query9_3()
        {
            var ticket = new Ticket();
            this.name = ticket.name;
            this.fields = ticket.fields;

            this.viewName = "Проданные билеты на определённый маршрут";
            this.queryParams = new List<QueryParametr>()
            {
               new QueryParametr("Route", "Route_id", DbTypes.DbRefference,
               "Маршрут", new Reference("Route", "Route_id", new string[]{"Route_id"}))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            string query;
            query = "SELECT Ticket.* FROM Station_timetable JOIN Ticket ON Ticket.Time_table_entry = Station_timetable.Entry_id WHERE (Route_id = ?) AND (Returned = 'N')";
            this.queryString = "(" + query + ")";
        }
    }
}
