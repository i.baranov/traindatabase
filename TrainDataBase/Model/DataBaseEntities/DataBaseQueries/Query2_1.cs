﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query2_1 : Query
    {
        public Query2_1()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники в указанной бригаде";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Brigade", "Brigade_id", DbTypes.DbRefference, 
                "Номер бригады", new Reference("Brigade", "Brigade_id", new string[] { "Brigade_number" }
                    ))
            };
            this.defaultValues = new List<string>()
            {
                "1"
            };

            this.paramsRefs = new string[] { "?" };
            this.queryString = "(" + "SELECT * FROM Worker WHERE Worker_id IN ( SELECT Worker_id FROM Brigade WHERE BRIGADE_NUMBER = ? ) " + ")";
        }
    }
}
