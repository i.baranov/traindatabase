﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query1_4 : Query
    {
        public Query1_4()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Работники по стажу работы";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Стаж (в годах) от"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "до")
            };
            this.defaultValues = new List<string>()
            {
                "0", "100"
            };

            this.paramsRefs = new string[] { "?", "??"};
            this.queryString = "(" + "WITH Experience_Worker_Null AS ( SELECT Worker_id , Round(((SELECT CURRENT_DATE From dual) - Start_date)/365,1) as Experience2 FROM Worker WHERE (End_date is NULL) ), Experience_Worker_Not_Null AS ( SELECT Worker_id , Round((End_date - Start_date)/365,1) as Experience1 FROM Worker ), Experience_Worker AS ( SELECT Worker_id as Worker, coalesce(Experience1,Experience2) as Experience FROM Experience_Worker_Not_Null LEFT JOIN Experience_Worker_Null USING (Worker_id) ), Needed_workers AS ( SELECT Worker FROM Experience_Worker WHERE (Experience> ?) AND (Experience < ??) ) SELECT * FROM Worker WHERE (Worker_id IN (SELECT Worker from Needed_workers))" + ")";
        }
    }
}
