﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities.DataBaseQueries
{
    class Query3_1 : Query
    {
        public Query3_1()
        {
            var worker = new Worker();
            this.name = worker.name;
            this.fields = worker.fields;
            this.viewName = "Водители, прошедшие или непрошедшие медосмотр в указанный год";
            this.queryParams = new List<QueryParametr>()
            {
                new QueryParametr("Worker", "Gender", DbTypes.DbBool, "Прошёл медосмотр?"),
                new QueryParametr("Worker", "Worker_id", DbTypes.DbInt, "Год")
            };
            this.defaultValues = new List<string>()
            {
                "Да", "2000"
            };

            this.paramsRefs = new string[] { "?", "??" };
            this.queryString = "(" + "WITH Needed_drivers AS ( SELECT * FROM Driver JOIN Body_check_history USING (Driver_id) WHERE (extract(year From Body_check_date) = ??) AND (Body_check_result = ?) ) SELECT * FROM Worker WHERE (Worker_id IN (SELECT Driver_id FROM Needed_drivers)) " + ")";
        }
    }
}
