﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainDataBase.DataBaseEntities
{
    enum TableId
    {
        Worker = 0,
        Manager = 1,
        Dispatcher = 2,
        Driver = 3,
        Body_check_history = 4,
        Brigade = 5,
        Locomotive = 6,
        Locomotive_check_history = 7,
        Locomotive_repair_history = 8,
        Train_type = 9,
        Passenger = 10,
        Department = 11,
        Route_types = 12,
        Railway_station = 13,
        Route_timetable = 14,
        Route = 15,
        Train = 16,
        Station_timetable = 17,
        Ticket = 18,
        Delayed_trip_history = 19,
        Delay_reason_type = 20
    }

    public class Table
    {
        public string name;
        public List<Field> fields;
        public string viewName;
    }
}
