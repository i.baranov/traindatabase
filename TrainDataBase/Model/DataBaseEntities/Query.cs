﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;

namespace TrainDataBase.Model.DataBaseEntities
{
    enum QueryId
    {
        Query1_1 = 0,
        Query1_2 = 1,
        Query1_3 = 2,
        Query1_4 = 3,
        Query1_5 = 4,
        Query1_6 = 5,
        Query2_1 = 6,
        Query2_2 = 7,
        Query2_3 = 8,
        Query2_4 = 9,
        Query2_5 = 10,
        Query3_1 = 11,
        Query3_2 = 12,
        Query4_1 = 13,
        Query4_2 = 14,
        Query4_3 = 15,
        Query5_1 = 16,
        Query5_2 = 17,
        Query5_3 = 18,
        Query5_4 = 19,
        Query6_1 = 20,
        Query6_2 = 21,
        Query6_3 = 22,
        Query7_1 = 23,
        Query7_2 = 24,
        Query7_3 = 25,
        Query8_1 = 26,
        Query8_2 = 27,
        Query8_3 = 28,
        Query8_4 = 29,
        Query9_1 = 30,
        Query9_2 = 31,
        Query9_3 = 32,
        Query9_4 = 33,
        Query9_5 = 34,
        Query9_6 = 35,
        Query9_7 = 36,
        Query9_8 = 37,
        Query10_1 = 38,
        Query10_2 = 39,
        Query11_1 = 40,
        Query11_2 = 41,
        Query11_3 = 42,
        Query11_4 = 43,
        Query11_5 = 44,
        Query11_6 = 45,
        Query12_1 = 46,
        Query12_2 = 47,
        Query12_3 = 48,
        Query13_1 = 49,
        Query13_2 = 50,
        Query13_3 = 51
    }

    class Query : Table
    {
        public List<QueryParametr> queryParams;
        public List<string> defaultValues;

        public string[] paramsRefs;
        public string queryString;
    }
}
