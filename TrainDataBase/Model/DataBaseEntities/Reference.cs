﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainDataBase.Model.DataBaseEntities
{
    public class Reference
    {
        public string tableName;
        public string refFieldName;
        public string[] viewFields;

        public Reference(string tableName, string refFieldName, string[] viewFields)
        {
            this.tableName = tableName;
            this.refFieldName = refFieldName;
            this.viewFields = viewFields;
        }
    }
}
