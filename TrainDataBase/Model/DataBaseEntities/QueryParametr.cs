﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainDataBase.DataBaseEntities;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase.Model.DataBaseEntities
{
    class QueryParametr : Field
    {
        public string tableName;

        public QueryParametr(string tableName, string name, DbTypes type, string viewName, Reference refference)
        {
            this.tableName = tableName;
            this.name = name;
            this.type = type;
            this.viewName = viewName;
            this.reference = refference;
        }
        
        public QueryParametr()
        {
            this.tableName = null;
            this.name = null;
            this.type = DbTypes.DbEmpty;
            this.viewName = "Запрос без параметров";
            this.reference = null;
        }

        public QueryParametr(string tableName, string name, DbTypes type, string viewName)
        {
            this.tableName = tableName;
            this.name = name;
            this.type = type;
            this.viewName = viewName;
            this.reference = null;
        }
    }
}
