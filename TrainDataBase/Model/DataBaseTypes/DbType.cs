﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TrainDataBase.Model.DataBaseTypes
{
    public enum DbTypes
    {
        DbBool = 1,
        DbDate = 2,
        DbFloat = 3,
        DbInt = 4,
        DbVarchar = 5,
        DbGender = 6,
        DbRefference = 7,
        DbKey = 8,
        DbTime = 9,
        DbComparison = 10,
        DbEmpty = 11
    }

    public class DbType
    {
        public string name;
    }
}