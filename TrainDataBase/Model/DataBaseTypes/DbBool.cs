﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainDataBase.Model.DataBaseTypes
{
    class DbBool : DbType
    {
        public DbBool()
        {
            name = "CHAR(1)";
        }
        
        public static string getView(string name)
        {
            if (name == "Y")
            {
                return "Да";
            }
            else
            {
                return "Нет";
            }
        }

        public static string getComp(string name)
        {
            if (name == "Да")
            {
                return "<>";
            }
            else
            {
                return "=";
            }
        }

            public static string getBool(string name)
        {
            if (name == "Да")
            {
                return "Y";
            }
            else
            {
                return "N";
            }
        }
    }
}
