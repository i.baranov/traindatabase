﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainDataBase.Model.DataBaseTypes
{
    class DbInt : DbType
    {
        public DbInt()
        {
            name = "Int";
        }
    }
}
