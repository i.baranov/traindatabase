﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TrainDataBase.Model.DataBaseTypes
{
    public class DbVarchar : DbType
    {
        public DbVarchar()
        {
            name = "VARCHAR2(255)";
        }
    }
}