﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainDataBase.Model.DataBaseTypes
{
    class DbGender
    {
        public static string getGender(string name)
        {
            if (name == "Y")
            {
                return "Муж.";
            }
            else
            {
                return "Жен.";
            }
        }

        public static string getBool(string name)
        {
            if (name == "Муж.")
            {
                return "Y";
            }
            else
            {
                return "N";
            }
        }
    }
}
