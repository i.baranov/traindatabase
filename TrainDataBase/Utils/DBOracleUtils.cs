﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TrainDataBase.Model.DataBaseTypes;

namespace TrainDataBase
{
    class DBOracleUtils
    {
        public static OracleConnection
                       GetDBConnection(string host, int port, String sid, String user, String password)
        {
            Console.WriteLine("Getting Connection ...");

            // 'Connection String' подключается напрямую к Oracle.
            string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                 + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "
                 + sid + ")));Password=" + password + ";User ID=" + user;


            OracleConnection conn = new OracleConnection();

            conn.ConnectionString = connString;

            return conn;
        }
        
        public static OracleConnection GetDBConnection(string user, string password)
        {
            string host = "127.0.0.1";
            int port = 1521;
            string sid = "XE";

            return DBOracleUtils.GetDBConnection(host, port, sid, user, password);
        }

        public static bool PerformQuery(OracleConnection connection, string query)
        {
            try
            {
                if (query.Length > 4000) return false;
                using (OracleCommand cmd = new OracleCommand(query, connection))
                {
                    Console.WriteLine("Performing query:" + "\r\n" + query);
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Error in query: \"" + ex.Message + "\" - with query " + query);
                return false;
            }
        }

        public static bool PerformQueryList(OracleConnection connection, List<string> queryList)
        {
            try
            {
                foreach (string query in queryList)
                {
                    if (!PerformQuery(connection, query))
                    {
                        return false;
                    };
                }
                return true;
            }
            catch (Exception exp)
            {
                Console.WriteLine("Error in query: \"" + exp.Message + "\"");
                return false;
            }
        }

        public static List<string> ReadData(OracleConnection connection, string query)
        {
            List<string> result = new List<string>();
            Console.WriteLine(query);
            using (OracleCommand cmd = new OracleCommand(query, connection))
            {
                using(OracleDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string entry;
                        try
                        {
                             entry = reader.GetString(0);
                        }
                        catch
                        {
                            entry = "-";
                        }
                        result.Add(entry);
                    }
                }
            }
            return result;
        }

        public static List<string> ReadAllData(OracleConnection connection, string query, int fieldsCount)
        {
            List<string> result = new List<string>();
            using (OracleCommand cmd = new OracleCommand(query, connection))
            {
                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (int i=0; i<fieldsCount; i++)
                        {
                            string entry;
                            try
                            {
                                entry = reader.GetString(i);
                            }
                            catch
                            {
                                entry = "-";
                            }
                            result.Add(entry);
                        }
                    }
                }
            }
            return result;
        }

        public static int GetCountOfRows(OracleConnection connection, string tableName)
        {
            List<string> result = new List<string>();

            string queryString = "SELECT COUNT(*) FROM " + tableName;
            result = ReadData(connection, queryString);

            return Convert.ToInt32(result[0]);
        }

        public static List<string> SelectAllDataFromTable(OracleConnection connection, string tableName, int fieldsCount)
        {
            List<string> result = new List<string>();
            string queryString = "SELECT * FROM " + tableName;
            result = ReadAllData(connection, queryString,fieldsCount);

            return result;
        }

        public static List<string> SelectEntity(OracleConnection connection, string tableName, string entityName)
        {
            List<string> result = new List<string>();

            string queryString = "SELECT " + entityName + " FROM " + tableName;
            result = ReadData(connection, queryString);

            return result;
        }

        public static List<string> SelectViewNames(OracleConnection connection, string tableName, string key, string keyName, string[] viewFields)
        {
            List<string> result = new List<string>();

            string viewNames = "";
            for (int i=0; i < viewFields.Length; i++)
            {
                if (i > 0) viewNames += ",";
                viewNames += viewFields[i];
            }

            string queryString = "SELECT " + viewNames + " FROM " + tableName + " WHERE " + keyName + "=" + key;
            result = ReadAllData(connection, queryString, viewFields.Length);

            return result;
        }

        public static List<string> SelectKeyWithClause(OracleConnection connection, string tableName, string key, string[] clauseFields, string[] values)
        {
            List<string> result = new List<string>();

            string clauseString = "";
            for (int i=0; i<clauseFields.Length; i++)
            {
                if (i > 0) clauseString += " AND ";
                clauseString += clauseFields[i] + "=" + "'" + values[i] + "'";
            }

            string queryString = "SELECT " + key + " FROM " + tableName + " WHERE " + clauseString;
            result = ReadData(connection, queryString);

            return result;
        }

        public static bool CreateTables(OracleConnection connection)
        {
            List<string> queryesList = new List<string>();

            //Создание работников
            queryesList.Add("CREATE TABLE Brigade (Worker_id INT NOT NULL, Brigade_id INT PRIMARY KEY,Brigade_number INT NOT NULL)");
            queryesList.Add("CREATE sequence BRIGADE_BRIGADE_ID_SEQ");
            queryesList.Add("CREATE trigger BI_BRIGADE_BRIGADE_ID before insert on Brigade for each row begin select BRIGADE_BRIGADE_ID_SEQ.nextval into :NEW.Brigade_id from dual; end;");

            //Создание бригад
            queryesList.Add("CREATE TABLE Worker (Worker_id INT PRIMARY KEY, First_name VARCHAR2(255) NOT NULL, Last_name VARCHAR2(255) NOT NULL, Gender CHAR(1) CHECK(Gender IN('N', 'Y')) NOT NULL, Birth_date DATE NOT NULL, Children_count INT, Salary FLOAT NOT NULL, Start_date DATE NOT NULL, End_date DATE, UNIQUE(First_name, Last_name))");
            queryesList.Add("CREATE sequence WORKER_WORKER_ID_SEQ");
            queryesList.Add("CREATE trigger BI_WORKER_WORKER_ID before insert on Worker for each row begin select WORKER_WORKER_ID_SEQ.nextval into :NEW.Worker_id from dual; end; ");

            //Отделы
            queryesList.Add("CREATE TABLE Department ( Department_id INT NOT NULL, Brigade_id INT NOT NULL, Station_id INT NOT NULL, Department_number INT NOT NULL, Manager_id INT NOT NULL, constraint DEPARTMENT_PK PRIMARY KEY (Department_id))");
            queryesList.Add("CREATE sequence DEPARTMENT_DEPARTMENT_ID_SEQ");
            queryesList.Add("CREATE trigger BI_DEPARTMENT_DEPARTMENT_ID before insert on Department for each row begin select DEPARTMENT_DEPARTMENT_ID_SEQ.nextval into :NEW.Department_id from dual; end;");

            //Локомотивы
            queryesList.Add("CREATE TABLE Locomotive ( Locomotive_id INT NOT NULL, Creation_date DATE NOT NULL, Brigade_id INT NOT NULL, constraint LOCOMOTIVE_PK PRIMARY KEY (Locomotive_id))");
            queryesList.Add("CREATE sequence LOCOMOTIVE_LOCOMOTIVE_ID_SEQ");
            queryesList.Add("CREATE trigger BI_LOCOMOTIVE_LOCOMOTIVE_ID before insert on Locomotive for each row begin select LOCOMOTIVE_LOCOMOTIVE_ID_SEQ.nextval into :NEW.Locomotive_id from dual; end;");

            //Станции
            queryesList.Add("CREATE TABLE Railway_station ( Station_id INT NOT NULL, Station_name VARCHAR2(255) NOT NULL, constraint RAILWAY_STATION_PK PRIMARY KEY (Station_id))");
            queryesList.Add("CREATE sequence RAILWAY_STATION_STATION_ID_SEQ");
            queryesList.Add("CREATE trigger BI_RAILWAY_STATION_STATION_ID before insert on Railway_station for each row begin select RAILWAY_STATION_STATION_ID_SEQ.nextval into :NEW.Station_id from dual; end;");

            //Водитель
            queryesList.Add("CREATE TABLE Driver ( Driver_id INT NOT NULL, Driver_class INT NOT NULL, constraint DRIVER_PK PRIMARY KEY (Driver_id))");
            //Диспетчер
            queryesList.Add("CREATE TABLE Dispatcher ( Dispatcher_Id INT NOT NULL, Main_dispacher CHAR(1) CHECK (Main_dispacher IN ('N','Y')) NOT NULL, constraint DISPATCHER_PK PRIMARY KEY (Dispatcher_Id))");
            //Поезд
            queryesList.Add("CREATE TABLE Train ( Train_id INT NOT NULL, Locomotive_id INT NOT NULL, Type_id INT NOT NULL, constraint TRAIN_PK PRIMARY KEY (Train_id))");
            queryesList.Add("CREATE sequence TRAIN_TRAIN_ID_SEQ");
            queryesList.Add("CREATE trigger BI_TRAIN_TRAIN_ID before insert on Train for each row begin select TRAIN_TRAIN_ID_SEQ.nextval into :NEW.Train_id from dual; end;");
            //Тип поезда
            queryesList.Add("CREATE TABLE Train_type ( Type_id INT NOT NULL, Type_name VARCHAR2(255) NOT NULL, constraint TRAIN_TYPE_PK PRIMARY KEY (Type_id))");
            queryesList.Add("CREATE sequence TRAIN_TYPE_TYPE_ID_SEQ");
            queryesList.Add("CREATE trigger BI_TRAIN_TYPE_TYPE_ID before insert on Train_type for each row begin select TRAIN_TYPE_TYPE_ID_SEQ.nextval into :NEW.Type_id from dual; end;");
            //Расписание станции
            queryesList.Add("CREATE TABLE Station_timetable ( Entry_id INT NOT NULL, Train_id INT NOT NULL, Arrival_date TIMESTAMP NOT NULL, Route_id INT NOT NULL, Delayed CHAR(1) CHECK (Delayed IN ('N','Y')) NOT NULL, Total_tickets INT NOT NULL, constraint STATION_TIMETABLE_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence STATION_TIMETABLE_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_STATION_TIMETABLE_ENTRY_ID before insert on Station_timetable for each row begin select STATION_TIMETABLE_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //Маршрут
            queryesList.Add("CREATE TABLE Route ( Route_id INT NOT NULL, Route_type INT NOT NULL, Train_number INT NOT NULL, constraint ROUTE_PK PRIMARY KEY (Route_id))");
            queryesList.Add("CREATE sequence ROUTE_ROUTE_ID_SEQ");
            queryesList.Add("CREATE trigger BI_ROUTE_ROUTE_ID before insert on Route for each row begin select ROUTE_ROUTE_ID_SEQ.nextval into :NEW.Route_id from dual; end;");
            //Типы маршрутов
            queryesList.Add("CREATE TABLE Route_types ( Route_type_id INT NOT NULL, Route_type_name VARCHAR2(255) NOT NULL, constraint ROUTE_TYPES_PK PRIMARY KEY (Route_type_id))");
            queryesList.Add("CREATE sequence ROUTE_TYPES_ROUTE_TYPE_ID_SEQ");
            queryesList.Add("CREATE trigger BI_ROUTE_TYPES_ROUTE_TYPE_ID before insert on Route_types for each row begin select ROUTE_TYPES_ROUTE_TYPE_ID_SEQ.nextval into :NEW.Route_type_id from dual; end;");
            //Билет
            queryesList.Add("CREATE TABLE Ticket ( Ticket_id INT NOT NULL, Passenger_id INT NOT NULL, Time_table_entry INT NOT NULL, Returned CHAR(1) CHECK (Returned IN ('N','Y')) NOT NULL, Booked CHAR(1) CHECK (Booked IN ('N','Y')) NOT NULL, Baggage CHAR(1) CHECK (Baggage IN ('N','Y')) NOT NULL, Ticket_price FLOAT NOT NULL, Start_station_id INT NOT NULL, End_station_id INT NOT NULL, constraint TICKET_PK PRIMARY KEY (Ticket_id))");
            queryesList.Add("CREATE sequence TICKET_TICKET_ID_SEQ");
            queryesList.Add("CREATE trigger BI_TICKET_TICKET_ID before insert on Ticket for each row begin select TICKET_TICKET_ID_SEQ.nextval into :NEW.Ticket_id from dual; end;");
            //Пассажиры
            queryesList.Add("CREATE TABLE Passenger ( Passenger_id INT NOT NULL, First_name VARCHAR2(255) NOT NULL, Last_name VARCHAR2(255) NOT NULL, Birth_date DATE NOT NULL, Gender CHAR(1) CHECK (Gender IN ('N','Y')) NOT NULL, constraint PASSENGER_PK PRIMARY KEY (Passenger_id), UNIQUE(First_name,Last_name))");
            queryesList.Add("CREATE sequence PASSENGER_PASSENGER_ID_SEQ");
            queryesList.Add("CREATE trigger BI_PASSENGER_PASSENGER_ID before insert on Passenger for each row begin select PASSENGER_PASSENGER_ID_SEQ.nextval into :NEW.Passenger_id from dual; end;");
            //Начальник
            queryesList.Add("CREATE TABLE Manager ( Manager_id INT NOT NULL, Manager_class INT NOT NULL, constraint MANAGER_PK PRIMARY KEY (Manager_id))");
            //Ремонт локомотивов
            queryesList.Add("CREATE TABLE Locomotive_repair_history ( Entry_id INT NOT NULL, Locomotive_id INT NOT NULL, Repair_date DATE NOT NULL, Repair_result CHAR(1) CHECK (Repair_result IN ('N','Y')) NOT NULL, constraint LOCOMOTIVE_REPAIR_HISTORY_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence LOCO_REPAIR_HIS_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_LOCOMOTD before insert on Locomotive_repair_history for each row begin select LOCO_REPAIR_HIS_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //Проверка локомотивов
            queryesList.Add("CREATE TABLE Locomotive_check_history ( Entry_id INT NOT NULL, Locomotive_id INT NOT NULL, Check_date DATE NOT NULL, Check_result CHAR(1) CHECK (Check_result IN ('N','Y')) NOT NULL, constraint LOCOMOTIVE_CHECK_HISTORY_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence LOCO_CHECK_HIS_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_LOCO_CHECK_HIS_ENTRY_ID before insert on Locomotive_check_history for each row begin select LOCO_CHECK_HIS_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //Проверка здоровья водителей
            queryesList.Add("CREATE TABLE Body_check_history ( Entry_id INT NOT NULL, Body_check_date DATE NOT NULL, Body_check_result CHAR(1) CHECK (Body_check_result IN ('N','Y')) NOT NULL, Driver_id INT NOT NULL, constraint BODY_CHECK_HISTORY_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence BODY_CHECK_HIS_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_BODY_CHECK_HIS_ENTRY_ID before insert on Body_check_history for each row begin select BODY_CHECK_HIS_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //Расписание по маршруту
            queryesList.Add("CREATE TABLE Route_timetable ( Entry_id INT NOT NULL, Route_id INT NOT NULL, Station_id INT NOT NULL, Time_from_start_station INT NOT NULL, Stop_time INT NOT NULL, constraint ROUTE_TIMETABLE_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence ROUTE_TIMETABLE_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_ROUTE_TIMETABLE_ENTRY_ID before insert on Route_timetable for each row begin select ROUTE_TIMETABLE_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //История задежек рейсов
            queryesList.Add("CREATE TABLE Delayed_trip_history ( Entry_id INT NOT NULL, Station_timetable_entry INT NOT NULL, Delay_reason_id INT NOT NULL, Delay_time INT, constraint DELAYED_TRIP_HISTORY_PK PRIMARY KEY (Entry_id))");
            queryesList.Add("CREATE sequence DELAYED_TRIP_HIS_ENTRY_ID_SEQ");
            queryesList.Add("CREATE trigger BI_DELAYED_TRIP_HIS_ENTRY_ID before insert on Delayed_trip_history for each row begin select DELAYED_TRIP_HIS_ENTRY_ID_SEQ.nextval into :NEW.Entry_id from dual; end;");
            //Причины задержек
            queryesList.Add("CREATE TABLE Delay_reason_type ( Reason_id INT NOT NULL, Reason VARCHAR2(255) NOT NULL,constraint DELAY_REASON_TYPE_PK PRIMARY KEY (Reason_id))");
            queryesList.Add("CREATE sequence DELAY_REAS_TYPE_REASON_ID_SEQ");
            queryesList.Add("CREATE trigger BI_DELAY_REASON_TYPE_REASON_ID before insert on Delay_reason_type for each row begin select DELAY_REAS_TYPE_REASON_ID_SEQ.nextval into :NEW.Reason_id from dual; end;");
            //Внешние ключи
            queryesList.Add("ALTER TABLE Brigade ADD CONSTRAINT Brigade_fk0 FOREIGN KEY (Worker_id) REFERENCES Worker(Worker_id)");
            queryesList.Add("ALTER TABLE Department ADD CONSTRAINT Department_fk0 FOREIGN KEY (Brigade_id) REFERENCES Brigade(Brigade_id)");
            queryesList.Add("ALTER TABLE Department ADD CONSTRAINT Department_fk1 FOREIGN KEY (Station_id) REFERENCES Railway_station(Station_id)");
            queryesList.Add("ALTER TABLE Department ADD CONSTRAINT Department_fk2 FOREIGN KEY (Manager_id) REFERENCES Worker(Worker_id)");
            queryesList.Add("ALTER TABLE Locomotive ADD CONSTRAINT Locomotive_fk0 FOREIGN KEY (Brigade_id) REFERENCES Brigade(Brigade_id)");
            queryesList.Add("ALTER TABLE Driver ADD CONSTRAINT Driver_fk0 FOREIGN KEY (Driver_id) REFERENCES Worker(Worker_id)");
            queryesList.Add("ALTER TABLE Dispatcher ADD CONSTRAINT Dispatcher_fk0 FOREIGN KEY (Dispatcher_Id) REFERENCES Worker(Worker_id)");
            queryesList.Add("ALTER TABLE Train ADD CONSTRAINT Train_fk0 FOREIGN KEY (Locomotive_id) REFERENCES Locomotive(Locomotive_id)");
            queryesList.Add("ALTER TABLE Train ADD CONSTRAINT Train_fk1 FOREIGN KEY (Type_id) REFERENCES Train_type(Type_id)");
            queryesList.Add("ALTER TABLE Station_timetable ADD CONSTRAINT Station_timetable_fk0 FOREIGN KEY (Train_id) REFERENCES Train(Train_id)");
            queryesList.Add("ALTER TABLE Station_timetable ADD CONSTRAINT Station_timetable_fk1 FOREIGN KEY (Route_id) REFERENCES Route(Route_id)");
            queryesList.Add("ALTER TABLE Route ADD CONSTRAINT Route_fk0 FOREIGN KEY (Route_type) REFERENCES Route_types(Route_type_id)");
            queryesList.Add("ALTER TABLE Ticket ADD CONSTRAINT Ticket_fk1 FOREIGN KEY (Time_table_entry) REFERENCES Station_timetable(Entry_id)");
            queryesList.Add("ALTER TABLE Ticket ADD CONSTRAINT Ticket_fk2 FOREIGN KEY (Start_station_id) REFERENCES Railway_station(Station_id)");
            queryesList.Add("ALTER TABLE Ticket ADD CONSTRAINT Ticket_fk3 FOREIGN KEY (End_station_id) REFERENCES Railway_station(Station_id)");
            queryesList.Add("ALTER TABLE Manager ADD CONSTRAINT Manager_fk0 FOREIGN KEY (Manager_id) REFERENCES Worker(Worker_id)");
            queryesList.Add("ALTER TABLE Locomotive_repair_history ADD CONSTRAINT Locomotive_repair_history_fk0 FOREIGN KEY (Locomotive_id) REFERENCES Locomotive(Locomotive_id)");
            queryesList.Add("ALTER TABLE Locomotive_check_history ADD CONSTRAINT Locomotive_check_history_fk0 FOREIGN KEY (Locomotive_id) REFERENCES Locomotive(Locomotive_id)");
            queryesList.Add("ALTER TABLE Body_check_history ADD CONSTRAINT Body_check_history_fk0 FOREIGN KEY (Driver_id) REFERENCES Driver(Driver_id)");
            queryesList.Add("ALTER TABLE Route_timetable ADD CONSTRAINT Route_timetable_fk0 FOREIGN KEY (Route_id) REFERENCES Route(Route_id)");
            queryesList.Add("ALTER TABLE Route_timetable ADD CONSTRAINT Route_timetable_fk1 FOREIGN KEY (Station_id) REFERENCES Railway_station(Station_id)");
            queryesList.Add("ALTER TABLE Delayed_trip_history ADD CONSTRAINT Delayed_trip_history_fk0 FOREIGN KEY (Station_timetable_entry) REFERENCES Station_timetable(Entry_id)");
            queryesList.Add("ALTER TABLE Delayed_trip_history ADD CONSTRAINT Delayed_trip_history_fk1 FOREIGN KEY (Delay_reason_id) REFERENCES Delay_reason_type(Reason_id)");

            return PerformQueryList(connection, queryesList);
        }

        public static bool DeleteTables(OracleConnection connection)
        {
            List<string> queryesList = new List<string>();

            queryesList.Add("DROP trigger BI_WORKER_WORKER_ID");
            queryesList.Add("DROP sequence WORKER_WORKER_ID_SEQ");
            queryesList.Add("DROP TABLE Worker");

            queryesList.Add("DROP trigger BI_BRIGADE_BRIGADE_ID");
            queryesList.Add("DROP sequence BRIGADE_BRIGADE_ID_SEQ");
            queryesList.Add("DROP TABLE Brigade");

            return PerformQueryList(connection, queryesList);
        }

        public static bool InsertData(OracleConnection connection)
        {
            List<string> queryesList = new List<string>();
            //queryesList.Add("INSERT ALL into Worker values(0, 'Андрей', 'Соколов', 'Y', '11-7-1968', 2, 38385, '7-10-2016', NULL) into Worker values(0, 'Егор', 'Михайлов', 'Y', '27-4-1984', 0, 24879, '26-9-2013', NULL) into Worker values(0, 'Александр', 'Фёдоров', 'Y', '9-12-1964', 0, 49368, '18-2-2017', NULL) into Worker values(0, 'Андрей', 'Кузнецов', 'Y', '26-1-1968', 2, 42200, '10-4-2015', NULL) into Worker values(0, 'Михаил', 'Васильев', 'Y', '5-9-1968', 2, 39018, '14-10-2017', NULL) into Worker values(0, 'Артем', 'Козлов', 'Y', '13-8-1979', 1, 26904, '8-4-2013', NULL) into Worker values(0, 'Кирилл', 'Захаров', 'Y', '18-3-1972', 1, 48163, '1-3-2019', NULL) into Worker values(0, 'Андрей', 'Петров', 'Y', '9-1-1957', 1, 42234, '12-10-2012', NULL) into Worker values(0, 'Настя', 'Мирная', 'N', '13-5-1984', 2, 19509, '21-5-2019', NULL) into Worker values(0, 'Маша', 'Сидорова', 'N', '13-5-1988', 2, 19669.0, '23-5-2016', '26-5-2018')SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Manager values(1, 1) into Manager values(2, 2) SELECT* FROM dual");
            //queryesList.Add("INSERT ALL into Dispatcher values (3, 'Y') into Dispatcher values (8, 'N') SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Driver values (3, 1) into Driver values (4, 2) into Driver values (5, 3) into Driver values (6, 2) into Driver values (7, 4) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Body_check_history values (0, '3-5-2018', 'Y', 3) into Body_check_history values (0, '27-3-2019', 'N', 4) into Body_check_history values (0, '14-5-2020', 'Y', 4) into Body_check_history values (0, '23-9-2016', 'Y', 5) into Body_check_history values (0, '21-6-2020', 'Y', 6) into Body_check_history values (0, '10-5-2015', 'Y', 7) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Brigade values (1, 0, 1) into Brigade values (2, 0, 2) into Brigade values (3, 0, 1) into Brigade values (4, 0, 1) into Brigade values (5, 0, 2) into Brigade values (6, 0, 2) into Brigade values (7, 0, 1) into Brigade values (8, 0, 2) into Brigade values (9, 0, 2) SELECT * FROM dual");
            // queryesList.Add("INSERT ALL into Locomotive values (0, '26-3-1952', 4) into Locomotive values (0, '3-9-1953', 4) into Locomotive values (0, '22-12-1967', 4) into Locomotive values (0, '7-4-1956', 5) into Locomotive values (0, '10-7-1980', 5) into Locomotive values (0, '23-5-1972', 5) into Locomotive values (0, '17-3-1983', 4) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Locomotive_check_history values (0, '5', '26-10-2015', 'N') into Locomotive_check_history values (0, '1', '13-1-1997', 'Y') into Locomotive_check_history values (0, '1', '16-8-1995', 'N') into Locomotive_check_history values (0, '3', '24-10-1991', 'N') into Locomotive_check_history values (0, '6', '11-10-2018', 'Y') into Locomotive_check_history values (0, '4', '26-1-2009', 'N') into Locomotive_check_history values (0, '7', '21-9-2002', 'Y') into Locomotive_check_history values (0, '5', '7-2-2015', 'Y') into Locomotive_check_history values (0, '4', '20-9-2004', 'Y') into Locomotive_check_history values (0, '6', '25-7-2016', 'N') into Locomotive_check_history values (0, '2', '14-8-1991', 'Y') into Locomotive_check_history values (0, '4', '26-2-1991', 'Y') into Locomotive_check_history values (0, '3', '21-7-2001', 'Y') into Locomotive_check_history values (0, '2', '21-8-2011', 'Y') into Locomotive_check_history values (0, '5', '25-10-1998', 'N') into Locomotive_check_history values (0, '2', '14-4-2000', 'Y') SELECT * FROM dual");
            //queryesList.Add("CREATE OR REPLACE Function Сount_stations ( start_station IN INTEGER, end_station IN INTEGER, route_id IN INTEGER) RETURN INTEGER IS stations_start_count INTEGER; stations_end_count INTEGER; cursor start_count is SELECT COUNT(Entry_id) FROM Route_timetable WHERE (Route_id = route_id) AND (Time_from_start_station >= (SELECT Time_from_start_station FROM Route_timetable WHERE (Station_id = start_station) AND (Route_id = route_id))); cursor end_count is SELECT COUNT(Entry_id) FROM Route_timetable WHERE (Route_id = route_id) AND (Time_from_start_station >= (SELECT Time_from_start_station FROM Route_timetable WHERE (Station_id = end_station) AND (Route_id = route_id))); BEGIN open start_count; fetch start_count into stations_start_count; close start_count; open end_count; fetch end_count into stations_end_count; close end_count; RETURN stations_start_count - stations_end_count; END;");
            //queryesList.Add("CREATE TRIGGER BI_TICKET_TICKET_PRICE before insert on Ticket for each row DECLARE route_id integer; begin SELECT Route_id INTO route_id FROM Station_timetable WHERE Entry_id = :new.Time_table_entry; select (Сount_stations(:new.Start_station_id, :new.End_station_id, route_id) * 100) into :new.Ticket_price from dual; end;");
            //queryesList.Add("INSERT ALL into Locomotive_repair_history values (0, '2', '22-4-2011', 'Y') into Locomotive_repair_history values (0, '6', '20-5-2004', 'Y') into Locomotive_repair_history values (0, '4', '8-1-1997', 'N') into Locomotive_repair_history values (0, '6', '21-6-2005', 'Y') into Locomotive_repair_history values (0, '1', '8-8-2002', 'Y') into Locomotive_repair_history values (0, '5', '14-5-2015', 'Y') into Locomotive_repair_history values (0, '6', '21-8-2006', 'Y') into Locomotive_repair_history values (0, '2', '20-4-2005', 'N') into Locomotive_repair_history values (0, '6', '22-9-2002', 'Y') into Locomotive_repair_history values (0, '2', '17-11-1996', 'Y') into Locomotive_repair_history values (0, '5', '21-3-2016', 'Y') into Locomotive_repair_history values (0, '1', '27-11-1992', 'Y') into Locomotive_repair_history values (0, '5', '4-10-2018', 'N') into Locomotive_repair_history values (0, '7', '1-7-2008', 'Y') into Locomotive_repair_history values (0, '4', '14-11-2005', 'Y') into Locomotive_repair_history values (0, '7', '3-9-2007', 'N') into Locomotive_repair_history values (0, '7', '23-3-2016', 'Y') into Locomotive_repair_history values (0, '7', '14-6-2016', 'N') into Locomotive_repair_history values (0, '3', '19-5-1992', 'N') into Locomotive_repair_history values (0, '1', '2-12-2005', 'Y') SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Train_type values (0, 'Скорый') into Train_type values (0, 'Пассажирский') into Train_type values (0, 'Туристический') into Train_type values (0, 'Экскурсионный') into Train_type values (0, 'Пригородный') SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Passenger values (0, 'Яков', 'Андреев', '2-6-1971', 'Y') into Passenger values (0, 'Артем', 'Попов', '2-9-1969', 'Y') into Passenger values (0, 'Егор', 'Павлов', '5-8-2014', 'Y') into Passenger values (0, 'Михаил', 'Никитин', '1-9-1966', 'Y') into Passenger values (0, 'Кирилл', 'Попов', '17-4-1996', 'Y') into Passenger values (0, 'Максим', 'Попов', '11-1-1982', 'Y') into Passenger values (0, 'Дмитрий', 'Егоров', '25-12-1963', 'Y') into Passenger values (0, 'Максим', 'Волков', '20-3-1971', 'Y') into Passenger values (0, 'Виктор', 'Иванов', '14-9-1981', 'Y') into Passenger values (0, 'Андрей', 'Макаров', '15-5-1957', 'Y') into Passenger values (0, 'Виктор', 'Николаев', '24-8-2003', 'Y') into Passenger values (0, 'Степан', 'Морозов', '20-4-1956', 'Y') into Passenger values (0, 'Илья', 'Лебедев', '3-9-1996', 'Y') into Passenger values (0, 'Даниил', 'Попов', '22-3-1985', 'Y') into Passenger values (0, 'Дмитрий', 'Николаев', '19-3-2010', 'Y') into Passenger values (0, 'Артем', 'Семенов', '13-12-1954', 'Y') into Passenger values (0, 'Егор', 'Васильев', '13-5-1972', 'Y') into Passenger values (0, 'Даниил', 'Степанов', '20-12-1956', 'Y') into Passenger values (0, 'Андрей', 'Захаров', '3-12-2014', 'Y') into Passenger values (0, 'Михаил', 'Новиков', '14-9-1967', 'Y') into Passenger values (0, 'Илья', 'Алексеев', '26-4-1989', 'Y') SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Route_types values (0, 'Внутpенние') into Route_types values (0, 'Междунаpодные') into Route_types values (0, 'Туристические') into Route_types values (0, 'Специальные') SELECT * FROM dual");
            // queryesList.Add("INSERT ALL into Railway_station values (0, 'МОСКОВСКАЯ') into Railway_station values (0, 'БЕЛОРУССКАЯ') into Railway_station values (0, 'ГОРЬКОВСКАЯ') into Railway_station values (0, 'ЮГО-ЗАПАДНАЯ') into Railway_station values (0, 'ЮЖНАЯ') into Railway_station values (0, 'ОКТЯБРЬСКАЯ') into Railway_station values (0, 'АРМЯНСКАЯ') into Railway_station values (0, 'КАЛИНИНГРАД') SELECT * FROM dual");
            // queryesList.Add("INSERT ALL into Department values (0, 4, 6, 1, 1) into Department values (0, 5, 6, 2, 10) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Route values (0, 1, 1) into Route values (0, 2, 2) into Route values (0, 3, 3) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Route_timetable values (0, 1, 1, 0, 15) into Route_timetable values (0, 1, 3, 60, 15) into Route_timetable values (0, 1, 8, 120, 15) into Route_timetable values (0, 2, 1, 0, 15) into Route_timetable values (0, 2, 6, 100, 15) into Route_timetable values (0, 2, 2, 250, 15) into Route_timetable values (0, 2, 7, 400, 15) into Route_timetable values (0, 3, 4, 0, 15) into Route_timetable values (0, 3, 5, 50, 15) into Route_timetable values (0, 3, 8, 180, 15) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Train values (0, 1, 1) into Train values (0, 2, 2) into Train values (0, 3, 3) into Train values (0, 4, 4) into Train values (0, 5, 5) into Train values (0, 6, 2) into Train values (0, 7, 1) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Station_timetable values (0, 2, TO_TIMESTAMP('2020-4-25 1:22','YYYY-MM-DD HH24:MI'), 1, 'N', 30) into Station_timetable values (0, 3, TO_TIMESTAMP('2020-4-20 7:46','YYYY-MM-DD HH24:MI'), 3, 'N', 30) into Station_timetable values (0, 4, TO_TIMESTAMP('2020-4-13 5:1','YYYY-MM-DD HH24:MI'), 3, 'N', 30) into Station_timetable values (0, 1, TO_TIMESTAMP('2020-4-19 9:6','YYYY-MM-DD HH24:MI'), 2, 'N', 50) into Station_timetable values (0, 7, TO_TIMESTAMP('2020-4-27 12:44','YYYY-MM-DD HH24:MI'), 2, 'N', 50) into Station_timetable values (0, 6, TO_TIMESTAMP('2020-4-25 1:7','YYYY-MM-DD HH24:MI'), 1, 'N', 50) into Station_timetable values (0, 5, TO_TIMESTAMP('2020-4-9 4:58','YYYY-MM-DD HH24:MI'), 3, 'N', 50) SELECT * FROM dual");
            // queryesList.Add("INSERT ALL into Ticket values (0, '15', '6', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '19', '5', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '2', '9', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '1', '9', 'N', 'N', 'N', 0, 6, 2) into Ticket values (0, '14', '10', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '8', '8', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '15', '8', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '7', '6', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '17', '9', 'N', 'N', 'N', 0, 1, 6) into Ticket values (0, '3', '10', 'N', 'N', 'N', 0, 3, 8) into Ticket values (0, '8', '10', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '6', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '11', '11', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '3', '11', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '18', '6', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '4', '6', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '11', '6', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '9', '10', 'N', 'N', 'N', 0, 1, 8) into Ticket values (0, '4', '11', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '20', '9', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '13', '11', 'N', 'N', 'N', 0, 5, 8) SELECT * FROM dual");
            // queryesList.Add("INSERT ALL into Ticket values (0, '5', '5', 'N', 'N', 'N', 0, 1, 8) SELECT * FROM dual");
            // queryesList.Add("Drop trigger BI_TICKET_TICKET_PRICE");
            //queryesList.Add("");
            //queryesList.Add("INSERT ALL into Ticket values (0, '5', '1', 'N', 'N', 'N', 0, 1, 8) into Ticket values (0, '15', '2', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '19', '1', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '2', '5', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '1', '5', 'N', 'N', 'N', 0, 6, 2) into Ticket values (0, '14', '6', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '8', '4', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '15', '4', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '7', '2', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '17', '5', 'N', 'N', 'N', 0, 1, 6) into Ticket values (0, '3', '6', 'N', 'N', 'N', 0, 3, 8) into Ticket values (0, '8', '6', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '6', '3', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '11', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '3', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '18', '2', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '4', '2', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '11', '2', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '9', '6', 'N', 'N', 'N', 0, 1, 8) into Ticket values (0, '4', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '20', '5', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '13', '7', 'N', 'N', 'N', 0, 5, 8) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Ticket values (0, '27', '1', 'N', 'N', 'N', 0, 1, 8) into Ticket values (0, '37', '2', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '41', '1', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '24', '5', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '23', '5', 'N', 'N', 'N', 0, 6, 2) into Ticket values (0, '36', '6', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '30', '4', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '37', '4', 'N', 'N', 'N', 0, 1, 7) into Ticket values (0, '29', '2', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '39', '5', 'N', 'N', 'N', 0, 1, 6) into Ticket values (0, '25', '6', 'N', 'N', 'N', 0, 3, 8) into Ticket values (0, '30', '6', 'N', 'N', 'N', 0, 1, 3) into Ticket values (0, '28', '3', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '33', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '25', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '40', '2', 'N', 'N', 'N', 0, 4, 8) into Ticket values (0, '26', '2', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '33', '2', 'N', 'N', 'N', 0, 5, 8) into Ticket values (0, '31', '6', 'N', 'N', 'N', 0, 1, 8) into Ticket values (0, '26', '7', 'N', 'N', 'N', 0, 4, 5) into Ticket values (0, '44', '5', 'N', 'N', 'N', 0, 1, 2) into Ticket values (0, '35', '7', 'N', 'N', 'N', 0, 5, 8) SELECT * FROM dual");
            //queryesList.Add("INSERT ALL into Delay_reason_type values (0, 'Опоздание поезда') into Delay_reason_type values (0, 'Погодные условия') into Delay_reason_type values (0, 'Технические неполадки') SELECT * FROM dual");

            return PerformQueryList(connection, queryesList);
        }
        
        public static int CountStations(OracleConnection connection, int end_station, int start_station, int route_id )
        {
            string queryesstr = "SELECT((SELECT COUNT(Entry_id) FROM Route_timetable WHERE(Route_id = "+route_id+") AND(Time_from_start_station >= (SELECT Time_from_start_station FROM Route_timetable WHERE(Station_id = "+start_station+") AND(Route_id = "+route_id+")))) -(SELECT COUNT(Entry_id) FROM Route_timetable WHERE(Route_id = "+route_id+") AND(Time_from_start_station >= (SELECT Time_from_start_station FROM Route_timetable WHERE(Station_id = "+end_station+") AND(Route_id = "+route_id+"))))) as result from dual";
            
            //queryesstr = "create or replace TRIGGER BI_TICKET_TICKET_PRICE before insert on Ticket for each row DECLARE route_id integer; begin SELECT Route_id INTO route_id FROM Station_timetable WHERE Station_timetable.Entry_id = :new.Time_table_entry; select (Сount_stations(:new.Start_station_id, :new.End_station_id, route_id) * 100) into :new.Ticket_price from dual; end;";

            var result = ReadData(connection, queryesstr);
           
            return Convert.ToInt32(result[0]) * 100;
        }

        public static bool InsertData(OracleConnection connection, string table, string data)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("INSERT ALL into " + table + " values(" + data + ") SELECT* FROM dual");

            Console.WriteLine("quary: " + queryesList[0]);

            return PerformQueryList(connection, queryesList);
        }

        public static bool InsertManager(OracleConnection connection, string data)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("BEGIN Insert_manager(" + data + "); END;");

            return PerformQueryList(connection, queryesList);
        }

        public static bool InsertDriver(OracleConnection connection, string data)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("BEGIN Insert_driver(" + data + "); END;");

            return PerformQueryList(connection, queryesList);
        }

        public static bool InsertDispacher(OracleConnection connection, string data)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("BEGIN Insert_dispacher(" + data + "); END;");

            return PerformQueryList(connection, queryesList);
        }

        public static bool InsertDelay(OracleConnection connection, string data)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("BEGIN Insert_delay(" + data + "); END;");

            return PerformQueryList(connection, queryesList);
        }

        public static bool EditData(OracleConnection connection, string table, string data, string keyName, string key)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("UPDATE " + table + " SET " + data + " WHERE " + keyName + "=" + key);

            Console.WriteLine("quary: " + queryesList[0]);

            return PerformQueryList(connection, queryesList);
        }

        public static bool DeleteRow(OracleConnection connection, string table, string keyName, string key)
        {
            List<string> queryesList = new List<string>();
            queryesList.Add("DELETE FROM " + table + " WHERE " + keyName + "=" + key);
        
            Console.WriteLine("quary: " + queryesList[0]);

            return PerformQueryList(connection, queryesList);
        }
    }
}